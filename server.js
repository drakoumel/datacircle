'use strict'

// server.js

// requires ====================================================================
const bodyParser = require('body-parser')
const config = require('config')
const cookieParser = require('cookie-parser') // eslint-disable-line
const express = require('express')
const expressValidator = require('express-validator')
const favicon = require('serve-favicon') // eslint-disable-line
const FileStreamRotator = require('file-stream-rotator')
const flash = require('connect-flash')
const fs = require('fs')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const morgan = require('morgan')
const multer = require('multer')
const path = require('path')
const session = require('express-session')
const uuid = require('node-uuid')
const nunjucks = require('express-nunjucks')
const passport = require('passport')
const Passport = require('./app/models/Passport')(passport) // eslint-disable-line
// end =========================================================================

// configuration ===============================================================
const $envMongoUIR = process.env.MONGOLAB_URI || config.get('mongoURI')
const $envLogDirectory = __dirname + config.get('logDirectory')
const $envPort = process.env.PORT || config.get('port')
const $envSecret = process.env.SECRET || config.get('secret')
// end =========================================================================

// database Initization ========================================================
mongoose.connect($envMongoUIR, function (err, res) {
  if (err) {
    console.log('ERROR connecting to: ' + $envMongoUIR + '. ' + err)
  } else {
    console.log('Succeeded connected to: ' + $envMongoUIR)
  }
})

require('./app/persistance/UserOptions')
// end =========================================================================

const app = express()
const router = express.Router()

app.set('view engine', 'html')
app.set('views', path.join(__dirname, 'views'))

// Configuring the template system.
nunjucks.setup({
  // (default: true) controls if output with dangerous characters are escaped automatically.
  autoescape: true,
  // (default: false) throw errors when outputting a null/undefined value.
  throwOnUndefined: false,
  // (default: false) automatically remove trailing newlines from a block/tag.
  trimBlocks: false,
  // (default: false) automatically remove leading whitespace from a block/tag.
  lstripBlocks: false,
  // (default: false) if true, the system will automatically update templates when they are changed on the filesystem.
  watch: true,
  // (default: false) if true, the system will avoid using a cache and templates will be recompiled every single time.
  noCache: true,
  // (default: see nunjucks syntax) defines the syntax for nunjucks tags.
  tags: {}
}, app)

app.set('port', $envPort)
fs.existsSync($envLogDirectory) || fs.mkdirSync($envLogDirectory)

const accessLogStream = FileStreamRotator.getStream({
  filename: $envLogDirectory + '/access-%DATE%.log',
  frequency: 'daily',
  verbose: true,
  // this date format is a hack for FileStreamRotator as the daily frequency is broken
  // TODO: https://github.com/holidayextras/file-stream-rotator/issues/9
  date_format: 'YYYYMMDD'
})

// setup the logger
app.use(morgan('combined', {stream: accessLogStream}))
app.use(methodOverride())
const sessionMiddleware = session({
  // store: new MongoStore({mongooseConnection: mongoose.connection}),
  resave: true,
  saveUninitialized: true,
  httpOnly: true,
  genid: function (req) {
    return uuid.v1() // use UUIDs for session IDs
  },
  secret: $envSecret,
  cookieName: 'session',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000
})

app.use(sessionMiddleware)
app.use(passport.initialize())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(multer())
app.use(express.static(path.join(__dirname, 'public')))
app.use(flash())
app.use(expressValidator())
app.use(favicon(__dirname + '/public/images/datacircle.ico'))

// Apply router && middleware
require('./app/middleware.js')(router, app.get('env'))
require('./app/routes.js')(router)
app.use('/', router)

// Error Handling ==============================================================
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use(function (err, req, res, next) {
  if (err.status === 404) {
    res.render('generic/404')
    return
  }

  if (app.get('env') === 'development') {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err,
      stack: err.stack
    })
  }

  if (app.get('env') === 'production') {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: 'err',
      stack: ''
    })
  }
})
// END Error Handling ==========================================================

app.locals.inspect = require('util').inspect

// Remove x-powered-by from response
app.disable('x-powered-by')

// launch ======================================================================
app.listen(app.get('port'), function () {
  console.log('Its time to pump up the jam! @ ' + app.get('port'))
})

app.on('error', onError)

function onError (error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  const bind = typeof port === 'string' ? 'Pipe ' + app.get('port') : 'Port ' + app.get('port')

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}
