// config/mailConfig.js
var fs = require('fs')
require.extensions['.html'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8')
}
var mainPath = '../config/mailTemplates/'

module.exports = {
  'accountRegistration': {
    'html': require(mainPath + 'accountRegistration.html'),
    'subject': 'subject'
  },
  'accountVerification': {
    'html': require(mainPath + './accountVerification.html'),
    'subject': 'DataCircle, Please confirm you email address.'
  },
  'invitation': {
    'html': require(mainPath + './invitation.html'),
    'subject': 'DataCircle, You have been invited!'
  }
}
