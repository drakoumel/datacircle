// config/production.js
var mailTemplates = require('../config/mailConfig.js')
var demoData = require('../config/demoData.js')

module.exports = {
  'url': 'http://localhost:8000/',
  'port': 8000,
  'mongoURI': 'mongodb://heroku_x835z903:5ipvud73iqcvpgdavrf3q17l9n@ds053972.mongolab.com:53972/heroku_x835z903?authMechanism=SCRAM-SHA-1',
  'logDirectory': '/log',
  'secret': 'development123',
  'chartType': {
    1: {
      name: 'line',
      fe: true,
      be: true
    },
    2: {
      name: 'bar',
      fe: true,
      be: true
    },
    3: {
      name: 'pie',
      fe: true,
      be: true
    },
    4: {
      name: 'donut',
      fe: true,
      be: true
    },
    5: {
      name: 'multiPie',
      fe: false,
      be: true
    }
  },
  'dataSourceType': {
    'mysql': 'MySql',
    'mongodb': 'MongoDB',
    'rabbitmq': 'RabbitMQ'
  },
  'mailgun': {
    'apiKey': 'key-5a8eb0226d2b5c26c2e4a02e7d7a3cfc',
    'domain': 'appa20f0425c354444eab7081548d8b9d74.mailgun.org',
    'publicKey': 'pubkey-0406b9752c7d2c83bf255dfe33309498',
    'login': 'postmaster@appa20f0425c354444eab7081548d8b9d74.mailgun.org',
    'paswword': 'cdd914fd81f46794ba64d2023a5d0ca4',
    'port': 587,
    'server': 'smtp.mailgun.org'
  },
  'mailTemplates': mailTemplates,
  'demoData': demoData
}
