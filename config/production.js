// config/production.js
var mailTemplates = require('../config/mailConfig.js')
var demoData = require('../config/demoData.js')

module.exports = {
  'url': 'https://www.datacircle.io/',
  'port': 8000,
  'mongoURI': 'mongodb://datacircleDummerAppUser:datacircleDummerAppUser@127.0.0.1:27017/datacircle?authMechanism=SCRAM-SHA-1',
  'logDirectory': '/log',
  'key': 'rftvdybdd8239!@!@c23939fn239r9mcmvapsASDW3ovmeefeWEDOemdoe',
  'secret': 'sdmo1232rdaSdbasgrEmvo223',
  'chartType': {
    1: {
      name: 'line',
      fe: true,
      be: true
    },
    2: {
      name: 'bar',
      fe: true,
      be: true
    },
    3: {
      name: 'pie',
      fe: true,
      be: true
    },
    4: {
      name: 'donut',
      fe: true,
      be: true
    },
    5: {
      name: 'multiPie',
      fe: false,
      be: true
    }
  },
  'dataSourceType': {
    'mysql': 'MySql',
    'mongodb': 'MongoDB',
    'rabbitmq': 'RabbitMQ'
  },
  'mailgun': {
    'apiKey': 'key-0604f8e40b66561d05196dd9ec2d685f',
    'publicKey': 'pubkey-276e950b080ec7fb57d5e551bc72b226',
    'domain': 'datacircle.io',
    'login': 'postmaster@datacircle.io',
    'paswword': 'd79efaab0f130901c0c8374db61bac1d',
    'port': 587,
    'server': 'smtp.mailgun.org'
  },
  'mailTemplates': mailTemplates,
  'demoData': demoData
}
