// config/default.js
module.exports = {
  'port': 8000,
  'mongoURI': 'mongodb://testuser:testuser@lennon.mongohq.com:10070/app24481810',
  'logDirectory': '/log',
  'secret': 'test',
  'chartType': { 1: 'line' },
  'dataSourceType': { 'mysql': 'MySql' }
}
