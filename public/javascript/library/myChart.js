/* globals Chart */
var chartMap = {}

function randomColor () {
  return '#' + Math.floor(Math.random() * 16777215).toString(16)
}

function ColorLuminance (hex, lum) {
  // validate hex string
  hex = String(hex).replace(/[^0-9a-f]/gi, '')
  if (hex.length < 6) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
  }
  lum = lum || 0

  // convert to decimal and change luminosity
  var rgb = '#'
  var c
  var i
  for (i = 0; i < 3; i++) {
    c = parseInt(hex.substr(i * 2, 2), 16)
    c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16)
    rgb += ('00' + c).substr(c.length)
  }

  return rgb
}

function hexToRgbA (hex, opacity) {
  var c
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split('')
    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    }
    c = '0x' + c.join('')
    return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + opacity + ')'
  }
  return false
}

function _chart (widgetId, type, data) {
  var canvasArray = decider(widgetId, type, data)
  var ctxArray = populateCtx(canvasArray)
  splitIntoSeperateCharts(canvasArray, ctxArray, type, data)
}

function splitIntoSeperateCharts (canvasArray, ctxArray, type, data) {
  for (var i = 0; i < canvasArray.length; i++) {
    var baseType = decideBaseType(type)
    var dataSet = data
    if (canvasArray.length > 1) {
      dataSet = data[i]
    }
    var chart = newChart(ctxArray[i], baseType, dataSet)
    cacheChartMap(canvasArray[i].id, chart, baseType, dataSet)

    setTimeout(function () {
      // document.getElementById('printChartBtn-').setAttribute('href', chart.toBase64Image())
    }, 200)
  }
}

function cacheChartMap (widgetId, chart, type, data) {
  chartMap[widgetId] = {
    'instance': chart,
    'type': type,
    'data': data
  }
}

function populateCtx (canvasArray) {
  var ctxArray = []
  for (var i = 0; i < canvasArray.length; i++) {
    ctxArray.push(canvasArray[i].getContext('2d'))
  }

  return ctxArray
}

function decider (id, type, data) {
  var canvasArray = []
  var mainCanvas = document.getElementById('myChart-' + id)
  canvasArray.push(mainCanvas)

  if (type === 5) {
    var smallDivParent = mainCanvas.parentElement
    var bigDivParent = smallDivParent.parentElement
    for (var i = 1; i < data[0].length; i++) {
      var tmp = mainCanvas.cloneNode()
      tmp.id = tmp.id + '-' + i
      canvasArray.push(tmp)
      var tmpSmallDivParent = smallDivParent.cloneNode()
      bigDivParent.appendChild(tmpSmallDivParent)
      tmpSmallDivParent.appendChild(tmp)
    }
  }

  canvasArray[0].id = canvasArray[0].id + '-0'

  return canvasArray
}

function decideBaseType (type) {
  if (type === 5) {
    return 3
  } else {
    return type
  }
}

function chartIt (widget) {
  try {
    _chart(widget._id, widget.chartType, widget.data)
  } catch (err) {
    showChartMsg(widget._id, 'Internal Error', err)
  }
}

function showChartMsg (widgetId, title, msg) {
  var card = document.querySelector('#card-' + widgetId)
  var titleElement = card.getElementsByClassName('chartNotificationTitle')[0]
  var msgElement = card.getElementsByClassName('chartNotificationMsg')[0]
  var notificationElement = card.getElementsByClassName('chartNotification')[0]

  titleElement.innerHTML = title
  msgElement.innerHTML = msg
  notificationElement.className = notificationElement.className.replace('hidden', '')
}

function sameData (oldData, newData, type) {
  var returnValue = true

  switch (type) {
    case 1:
    case 2:
      if (oldData.x.length !== newData.x.length || oldData.y.length !== newData.y.length) {
        returnValue = false
      }
      oldData.x.forEach(function (value, index) {
        if (newData.x[index] !== value) {
          returnValue = false
        }
      })
      oldData.y[1].forEach(function (value, index) {
        if (newData.y[1][index] !== value) {
          returnValue = false
        }
      })
      break
    case 3:
    case 4:
      if (oldData.length !== newData.length) {
        returnValue = false
      }

      oldData.forEach(function (val, index) {
        if (val.label !== newData[index].label) {
          returnValue = false
        }
        if (val.value !== newData[index].value) {
          returnValue = false
        }
      })
      break
  }

  return returnValue
}

function refreshChart (widgetId, type, data) {
  if (type === 5) {
    for (var i = 0; i < data.length; i++) {
      var innerWidgetId = 'myChart-' + widgetId + '-' + i
      var type = chartMap[innerWidgetId].type
      var newDataSet = data[i]

      var isItSame = sameData(chartMap[innerWidgetId].data, newDataSet, type)
      if (!isItSame) {
        chartMap[innerWidgetId].data = newDataSet

        switch (type) {
          case 1:
            var newLineChart = lineChart(data)
            chartMap[innerWidgetId].instance.data.datasets = newLineChart.datasets
            chartMap[innerWidgetId].instance.data.labels = newLineChart.labels
            chartMap[innerWidgetId].instance.update()
            break
          case 2:
            var newBarChart = barChart(data)
            chartMap[innerWidgetId].instance.data.datasets = newBarChart.datasets
            chartMap[innerWidgetId].instance.data.labels = newBarChart.labels
            chartMap[innerWidgetId].instance.update()
            break
          case 3:
            chartMap[innerWidgetId].instance.data.datasets = pieChart(newDataSet).datasets
            chartMap[innerWidgetId].instance.update()
            break
          case 4:
            chartMap[innerWidgetId].instance.data = donutChart(newDataSet)
            chartMap[innerWidgetId].instance.update()
            break
          default:
        }
      }
    }

  } else {
    var widgetId = 'myChart-' + widgetId + '-0'
    var type = chartMap[widgetId].type

    var isItSame = sameData(chartMap[widgetId].data, data, type)
    if (!isItSame) {
      chartMap[widgetId].data = data

      switch (type) {
        case 1:
          var newLineChart = lineChart(data)
          chartMap[widgetId].instance.data.datasets = newLineChart.datasets
          chartMap[widgetId].instance.data.labels = newLineChart.labels
          chartMap[widgetId].instance.update()
          break
        case 2:
          var newBarChart = barChart(data)
          chartMap[widgetId].instance.data.datasets = newBarChart.datasets
          chartMap[widgetId].instance.data.labels = newBarChart.labels
          chartMap[widgetId].instance.update()
          break
        case 3:
          chartMap[widgetId].instance.data.datasets = pieChart(data).datasets
          chartMap[widgetId].instance.update()
          break
        case 4:
          chartMap[widgetId].instance.data = donutChart(data)
          chartMap[widgetId].instance.update()
          break
        default:
      }
    }
  }
}

function newChart (ctx, type, data) {
  var chart
  switch (type) {
    case 1:
      chart = new Chart(ctx, {type: 'line', data: lineChart(data)})
      break
    case 2:
      chart = new Chart(ctx, {type: 'bar', data: barChart(data)})
      break
    case 3:
      chart = new Chart(ctx, {type: 'pie', data: pieChart(data)})
      break
    case 4:
      chart = new Chart(ctx, {type: 'doughnut', data: donutChart(data)})
      break
    default:
  }

  return chart
}

function donutChart (dataSet) {
  var labels = []
  var ds = {
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }

  dataSet.forEach(function (dataRow) {
    labels.push(dataRow.label)
    ds.data.push(dataRow.value)
    var color = randomColor()
    ds.backgroundColor.push(color)
    ds.hoverBackgroundColor.push(ColorLuminance(color, -0.2))
  })

  return {
    labels: labels,
    datasets: [ds]
  }
}

function pieChart (dataSet) {
  return donutChart(dataSet)
}

function barChart (dataSet) {
  var data = {
    labels: dataSet.x,
    datasets: []
  }

  dataSet.y.forEach(function (dataRow) {
    if (dataRow) {
      var baseHexColor
      var baseRgba = false

      do {
        baseHexColor = randomColor()
        baseRgba = hexToRgbA(baseHexColor, 0.2)
      } while (!baseRgba)

        var lineColor = ColorLuminance(baseHexColor, 0.2) // darker

        data.datasets.push({
          label: "My First dataset",
          backgroundColor: randomColor(),
          //borderColor: lineColor,
          //borderWidth: 1,
          //hoverBackgroundColor: randomColor(),
          hoverBorderColor: "rgba(255,99,132,1)",

          // pointColor: lineColor,
          // pointStrokeColor: '#fff',
          // pointHighlightFill: '#fff',
          // pointHighlightStroke: 'gray',
          data: dataRow
        })
      }
    })

    return data
  }

  function lineChart (dataSet) {
    var data = {
      labels: dataSet.x,
      datasets: []
    }

    dataSet.y.forEach(function (dataSet) {
      if (dataSet) {
        var baseHexColor = randomColor()
        var baseRgba = hexToRgbA(baseHexColor, 0.2)
        var lineColor = ColorLuminance(baseHexColor, -0.2) // darker

        data.datasets.push({
          fill: false,
          // backgroundColor
          borderColor: lineColor,
          borderCapStyle: 'butt',
          pointColor: lineColor,
          pointStrokeColor: '#fff',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'gray',
          data: dataSet
        })
      }
    })

    return data
  }
