function alarm (type, message, timer) {
  if (typeof timer === 'undefined') {
    timer = 3000
  }

  switch (type) {
    case 'success':
    case 1:
    case true:
      var alarmSuccessClone = $('#alarmSuccessClone').clone()
      $(alarmSuccessClone).attr('id', 'random')
      $(alarmSuccessClone).find('#message').html(message)
      $(alarmSuccessClone).fadeIn(1000)
      $('#alarmPlaceholder').append(alarmSuccessClone)
      setTimeout(function () {
        $(alarmSuccessClone).hide('slow', function () {
          $(alarmSuccessClone).remove()
        })
      }, timer)
      break
    case 'error':
    case 'failure':
    case 0:
    case false:
      var alarmErrorClone = $('#alarmErrorClone').clone()
      $(alarmErrorClone).attr('id', 'random')
      $(alarmErrorClone).find('#message').html(message)
      $(alarmErrorClone).fadeIn(1000)
      $('#alarmPlaceholder').append(alarmErrorClone)
      setTimeout(function () {
        $(alarmErrorClone).hide('slow', function () {
          $(alarmErrorClone).remove()
        })
      }, timer)
      break
    default:
  }
}

function randomNumber (max, min) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
