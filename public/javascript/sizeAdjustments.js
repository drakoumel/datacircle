function adjustScreenSize () {
  var w = window
  var d = document
  var e = d.documentElement
  var g = d.getElementsByTagName('body')[0]
  // var x = w.innerWidth || e.clientWidth || g.clientWidth
  var y = w.innerHeight || e.clientHeight || g.clientHeight

  var element = document.querySelector('footer')
  var rect = element.getBoundingClientRect()

  if (y > rect.bottom) {
    var value = (y - rect.bottom) + 20
    element.style.marginTop = value.toString() + 'px'
  }
}

adjustScreenSize()
