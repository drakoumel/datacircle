/* global $,Chart */

$(function () {
  $('.button-collapse').sideNav()
  magicNaviation()
  BindNavigation()
  showCookieHeader()
  Chart.defaults.global.responsive = true
// ChartInit()
})

function magicNaviation () {
  $('*[id*=nav-Action-]').click(function (element) {
    var elementId = element.currentTarget.id
    var panelName = elementId.substring(elementId.lastIndexOf('-') + 1, elementId.length)
    var panelElement = $('#' + panelName + 'Panel')

    if (panelElement.length) {
      if (panelElement.is(':visible')) {
        $('*[id*=Panel]:visible').hide()
        $('#index-banner').fadeIn()
        $('#sidenav-overlay').click()
      } else {
        $('*[id*=Panel]:visible').hide()
        $('#index-banner').hide()
        panelElement.fadeIn()
        $('#sidenav-overlay').click()
      }
    }
  })

  if (window.location.hash.length > 1) {
    var panelName = window.location.hash.replace('#', '').capitalizeFirstLetter()
    var panelElement = $('#' + panelName + 'Panel')

    if (panelElement.length) {
      $('*[id*=Panel]:visible').hide()
      $('#index-banner').hide()
      panelElement.fadeIn()
      $('#sidenav-overlay').click()
    }
  }
}

function BindNavigation () {
  $('#nav-Action-Mobile-Store').click(function () {
    $('#store').click()
  })

  $('#nav-Action-Store').click(function () {
    $('#store').click()
  })
}

String.prototype.capitalizeFirstLetter = function () {
  return this.charAt(0).toUpperCase() + this.slice(1)
}

var QueryString = function () { // eslint-disable-line
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {}
  var query = window.location.search.substring(1)
  var vars = query.split('&')
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=')
    // If first entry with this name
    if (typeof query_string[pair[0]] === 'undefined') {
      query_string[pair[0]] = decodeURIComponent(pair[1])
    // If second entry with this name
    } else if (typeof query_string[pair[0]] === 'string') {
      var arr = [ query_string[pair[0]], decodeURIComponent(pair[1]) ]
      query_string[pair[0]] = arr
    // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]))
    }
  }
  return query_string
}()

function showCookieHeader () {
  if ((document.cookie.match(/^(?:.*;)?cookieagree=([^;]+)(?:.*)?$/) || [, null])[1] == null) { // eslint-disable-line
    $('#cookie-header').show()
  }
}

function storeCookie () { // eslint-disable-line
  var d = new Date()
  d.setTime(d.getTime() + 5 * 60 * 1000) // in milliseconds
  document.cookie = 'cookieagree=check;path=/;expires=' + d.toGMTString() + ';'
}
