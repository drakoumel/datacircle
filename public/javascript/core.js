/* globals angular,$,alarm,QueryString,chartIt,refreshChart,showChartMsg */

var DataCircle = angular.module('DataCircle', []).config(function ($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}')
})

DataCircle
  .directive('loadRegisterHash', function ($compile) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        ngModel: '='
      },
      link: function (scope, element, attrs, ngModel) {
        if (QueryString.registerHash !== undefined) {
          scope.ngModel = QueryString.registerHash
        }
      }
    }
  })

DataCircle
  .directive('loadCompany', function ($compile) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        ngModel: '='
      },
      link: function (scope, element, attrs, ngModel) {
        if (QueryString.company !== undefined) {
          scope.ngModel = QueryString.company
          element.attr('readonly', 'readonly')
        }
      }
    }
  })

DataCircle.run(function ($rootScope) {
  $rootScope.$on('emitAddWidget', function (event, args) {
    $rootScope.$broadcast('handleAddWidget', args)
  })
  $rootScope.$on('emitPrepareDashboard', function (event, args) {
    $rootScope.$broadcast('handlePrepareDashboard', args)
  })
})

DataCircle.service('sharedProperties', function ($http) {
  var availableWidgets
  var selectedDashboard
  var availableDashboards = []
  return {
    getAvailableWidgets: function () {
      return availableWidgets
    },
    setAvailableWidgets: function (value) {
      availableWidgets = value
    },
    setAvailableDashboards: function (value) {
      availableDashboards = value
    },
    getAvailableDashboards: function () {
      return availableDashboards
    },
    setSelectedDashboard: function (value) {
      if (value._id) {
        $http.post('/updateSelectedDashbaord', { dashboardId: value._id })
      }
      selectedDashboard = value
    },
    getSelectedDashboard: function () {
      return selectedDashboard
    }
  }
})

DataCircle.controller('store', function ($scope, $http, sharedProperties) {
  $scope.addWidget = function (widgetId, callback) {
    $('#store').click()
    if (sharedProperties.getSelectedDashboard() === undefined) {
      alarm('error', 'Please create a Dashboard First!')
      return
    }

    $http.put('/dashboard/addWidget', { widgetId: widgetId, dashboardId: sharedProperties.getSelectedDashboard()._id })
      .success(function (data) {
        if (data.code === 'success') {
          $scope.$emit('emitAddWidget', widgetId)
          alarm(data.code, data.message)
        }
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }
})

DataCircle.controller('mainController', function ($scope, $http, sharedProperties) {
  $scope.acknowledgement = function (callback) {
    $http.get('/acknowledgement')
      .success(function (result) {
        $scope.acknowledgement = result
        callback()
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.updateSelectedDataSource = function (id) {
    if (id !== undefined) {
      $scope.availableDataSources.forEach(function (value, index) {
        if (value._id === id) {
          $scope.selectedDataSource = value
        }
      })
    }
  }

  // TODO THIS IS CALLED ON PAGE LOAD, MAKE IT ONLY DURING LOGIN!
  $scope.acknowledgement(function () {
    (function () {
      if (QueryString.registerHash !== undefined) {
        $('#nav-Register').click()
      }
    }())

    $scope.getRoles()
    $scope.getChartTypes()
    $scope.getAvailableDataSources()
    $scope.getDataSourceTypes()
    $scope.getAvailableWidgets()
    $scope.getDashboards()
  })

  $scope.updateOrCreateDashboard = function () {
    $http.put('/dashboard', $scope.dashboardFormData)
      .success(function (data) {
        alarm(data.code, data.message)
        document.getElementById('nav-Action-Dashboard').click()
        $scope.dashboardFormData = {}
        if (!sharedProperties.getSelectedDashboard() && data.data) {
          sharedProperties.setSelectedDashboard(data.data)
        }
        $scope.getDashboards()
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.getSelectedDashboard = function () {
    return sharedProperties.getSelectedDashboard()
  }

  $scope.userInvite = function () {
    $http.post('/userinvite', $scope.userInviteFormData)
      .success(function (data) {
        alarm(data.code, data.message)
        $scope.userInviteFormData = {}
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.getDetailsForWidget = function () {
    $http.get('/detailsForWidget/' + $('#drpdEditWidget').val())
      .success(function (result) {
        $scope.widgetFormData = result.data
        var collective = result.data.collectives[0]
        $scope.widgetFormData.query = collective.query

        if (collective.rabbitmq) {
          $scope.widgetFormData.rabbitmq = {}
          $scope.widgetFormData.rabbitmq.names = collective.rabbitmq.names
          $scope.widgetFormData.rabbitmq.limits = collective.rabbitmq.limits
        }
        $scope.widgetFormData.availableDataSource = collective.datasource._id
        $scope.updateSelectedDataSource(collective.datasource._id)
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.getDetailsForDataSource = function () {
    $http.get('/detailsForDataSource/' + $('#drpdEditDS').val())
      .success(function (result) {
        $scope.dataSourceFormData = result.data
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.updateOrCreateDataSource = function () {
    $http.post('/updateOrCreateDataSource', $scope.dataSourceFormData)
      .success(function (data) {
        alarm(data.code, data.message)
        $scope.dataSourceFormData = {}
        $scope.getAvailableDataSources()
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.getChartTypes = function () {
    $http.get('/chartType')
      .success(function (result) {
        $scope.chartTypes = result.data
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.$on('handleAddWidget', function (event, args) {
    $scope.getDashboards()
  })

  $scope.getDashboards = function () {
    $http.get('/dashboard/getAll')
      .success(function (result) {
        if (result.data && result.data.length > 0) {
          $scope.availableDashboards = result.data
          sharedProperties.setAvailableDashboards($scope.availableDashboards)
          if (sharedProperties.getSelectedDashboard() === undefined) {
            sharedProperties.setSelectedDashboard(result.data[0])
          } else {
            result.data.forEach(function (dashboard, index) {
              if (dashboard._id === sharedProperties.getSelectedDashboard()._id) {
                sharedProperties.setSelectedDashboard(dashboard)
              }
            })
          }
          $scope.$emit('emitPrepareDashboard')
        }
      })
  }

  $scope.getAvailableWidgets = function () {
    $http.get('/availableWidgets')
      .success(function (result) {
        $scope.availableWidgets = result.data
        sharedProperties.setAvailableWidgets($scope.availableWidgets)
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.getAvailableDataSources = function () {
    $http.get('/datasources')
      .success(function (result) {
        if (result.data && result.data.length === 0) {
          $('#drpdEditDS').hide()
          document.querySelector('#emptyDash').className = document.querySelector('#emptyDash').className.replace('hidden', '')
        } else {
          $('#drpdEditDS').show()
        }
        $scope.availableDataSources = result.data
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.getDataSourceTypes = function () {
    $http.get('/dataSourceType')
      .success(function (result) {
        $scope.dataSourceTypes = result.data
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.updateOrCreateWidget = function () {
    for (var index in $scope.availableDataSources) {
      if ($scope.availableDataSources[index]._id === $scope.widgetFormData.availableDataSource) {
        $scope.widgetFormData.type = $scope.availableDataSources[index].type
      }
    }

    $http.post('/updateOrCreateWidget', $scope.widgetFormData)
      .success(function (data) {
        alarm(data.code, data.message)
        $scope.widgetFormData = {}
        $scope.getAvailableWidgets()
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.deleteWidget = function () {
    alarm('error', 'Currently widget deletion is not available.')
    return

    if ($scope.widgetFormData._id === undefined) {
      alarm('error', 'No widget selected.')
    } else {
      $http.delete('/widget/' + $scope.widgetFormData._id)
        .success(function (data) {
          alarm(data.code, data.message)
          $scope.$emit('emitPrepareDashboard')
        })
        .error(function (data) {
          alarm(data.code, data.message)
          console.log('Error: ' + data)
        })
    }
  }

  $scope.getRoles = function () {
    $http.get('/roles')
      .success(function (data) {
        if (data.data) {
          $scope.availableRoles = data.data
        } else {
          $scope.availableRoles = {}
        }
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.createOrUpdateRole = function () {
    $http.put('/role', $scope.roleFormData)
      .success(function (data) {
        alarm(data.code, data.message)
        $scope.roleFormData = {}
      })
      .error(function (data) {
        alarm(data.code, data.message)
        console.log('Error: ' + data)
      })
  }

  $('#drpdEditRole').change(function () {
    $scope.availableRoles.forEach(function (role) {
      if (role._id === $('#drpdEditRole').val()) {
        $scope.roleFormData = {}
        $scope.roleFormData._id = role._id
        $scope.roleFormData.name = role.name
        $scope.roleFormData.admin = role.access.admin
        $scope.roleFormData.widgetRead = role.access.widgetRead
        $scope.roleFormData.widgetWrite = role.access.widgetWrite
        $scope.roleFormData.datasourceRead = role.access.datasourceRead
        $scope.roleFormData.datasourceWrite = role.access.datasourceWrite
        $scope.roleFormData.dashboardRead = role.access.dashboardRead
        $scope.roleFormData.dashboardWrite = role.access.dashboardWrite
      }
    })
  })

  $('#drpdEditDS').change(function () {
    $scope.getDetailsForDataSource()
  })

  $scope.selecteDashboard = function (dashboard) {
    sharedProperties.setSelectedDashboard(dashboard)
  }
})

DataCircle.controller('dashboard', function ($scope, $http, $interval, sharedProperties) {
  $scope.selectedDashboard = []
  sharedProperties.dashboard = []
  $scope.intervals = []

  $scope.refreshWidget = function (widgetId, type) {
    $http.get('/widgetData/' + widgetId)
      .success(function (data) {
        if (data.code === 'success') {
          refreshChart(widgetId, type, data.data)
        } else if (data.code === 'error') {
          showChartMsg(widgetId, 'Error', data.msg || data.message)
        }
      })
      .error(function (data) {
        alarm(data.code, data.message)
      })
  }

  $scope.removeWidget = function (widgetId) {
    $http.post('/dashBoard/removewidget', { widgetId: widgetId, dashboardId: sharedProperties.getSelectedDashboard()._id })
      .success(function (result) {
        $interval.cancel($scope.intervals[widgetId])
        delete $scope.intervals[widgetId]
        $scope.$emit('emitAddWidget')
      })
      .error(function (data) {
        console.log('Error: ' + data)
      })
  }

  $scope.requestData = function (key) {
    var widgetInQuestion = $scope.selectedDashboard.widgets[key]

    if (widgetInQuestion) {
      $http.get('/widgetData/' + widgetInQuestion._id)
        .success(function (result) {
          $scope.selectedDashboard.widgets[key].data = result.data
          $scope.createChart($scope.selectedDashboard.widgets[key])
        })
        .error(function (data) {
          console.log('Error: ' + data)
        })
    }
  }

  $scope.createChart = function (widget) {
    var refreshRate = widget.refreshRate || 10000
    chartIt(widget)
    if ($scope.intervals[widget._id] === undefined) {
      $scope.intervals[widget._id] = $interval(function () { $scope.refreshWidget(widget._id, widget.chartType) }, refreshRate)
    }
  }

  $scope.updateWidgets = function () {
    if ($scope.dashboardCharts.length > 0) {
      $('#chartSection').show()
    } else {
      $('#chartSection').hide()
    }
  }

  $scope.$on('handlePrepareDashboard', function (event, args) {
    $scope.selectedDashboard = sharedProperties.getSelectedDashboard()
  })
})
