'use strict'
// updated_at.js

var crypto = require('crypto')

exports.lastModified = function (schema, options) {
  schema.add({ updated_at: Date })

  schema.pre('save', function (next) {
    this.updated_at = new Date()
    next()
  })

  if (options && options.index) {
    schema.path('updated_at').index(options.index)
  }
}

exports.emailVerification = function (schema) {
  schema.add({
    verification: {
      key: { type: String, default: null },
      created_at: { type: Date, default: null },
      activated: { type: Boolean, default: false }
    }
  })

  schema.pre('save', function (next) {
    this.created_at = new Date()
    this.activated = false
    this.verification.key = crypto.randomBytes(64).toString('hex')
    next()
  })
}
