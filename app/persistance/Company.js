'use strict'
// models/Company.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = new mongoose.Schema({
  members: { type: Array, default: [], ref: 'User' },

  name: { type: String, default: null },
  description: { type: String, default: null },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.statics.findCompany = function (name) {
  return this.findOne({ name: new RegExp(name, 'i') })
}

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('Company', schema)
