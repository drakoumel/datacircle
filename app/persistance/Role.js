'use strict'
// models/Role.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = mongoose.Schema({
  access: {
    admin: { type: Boolean, default: false },
    widgetRead: { type: Boolean, default: false },
    widgetWrite: { type: Boolean, default: false },
    datasourceRead: { type: Boolean, default: false },
    datasourceWrite: { type: Boolean, default: false },
    dashboardRead: { type: Boolean, default: false },
    dashboardWrite: { type: Boolean, default: false }
  },

  owner: { type: Schema.Types.ObjectId, default: null, ref: 'User' },
  fkcompany: { type: Schema.Types.ObjectId, default: null, ref: 'Company' },
  name: { type: String },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
})

schema.statics.findRole = function (company, name) {
  return this.findOne({ 'company': company, 'name': name })
}

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('Role', schema)
