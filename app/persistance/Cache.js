'use strict'
// models/Cache.js
const mongoose = require('mongoose')

let schema = mongoose.Schema({
  widgetId: { type: String, default: '' },
  data: { type: Object },
  createdAt: { type: Date, default: Date.now, expires: 30 },
  updateAt: { type: Date, default: Date.now }
})

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('Cache', schema)
