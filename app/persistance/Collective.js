'use strict'
// models/Collective.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = mongoose.Schema({
  datasource: { type: Schema.Types.ObjectId, ref: 'Datasource' },
  query: { type: String },
  rabbitmq: {
    names: { type: String },
    limits: { type: String }
  },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('Collective', schema)
