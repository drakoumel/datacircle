'use strict'
// models/Widget.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = new mongoose.Schema({
  refreshRate: { type: Number, default: 5000 },
  chartType: { type: Number },
  collectives: [{ type: Schema.Types.ObjectId, ref: 'Collective' }],

  name: { type: String, default: null },
  description: { type: String, default: null },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.statics.findWidget = function (id) {
  return this.findOne({ '_id': id })
}

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('Widget', schema)
