'use strict'
// models/UserOption.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = mongoose.Schema({
  selectedDashboard: { type: Schema.Types.ObjectId, ref: 'Dashboard' },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('UserOptions', schema)
