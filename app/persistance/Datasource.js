'use strict'
// models/Datasource.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = new mongoose.Schema({
  type: { type: String },
  mysql: {
    connectionString: { type: String },
    host: { type: String },
    username: { type: String },
    password: { type: String },
    database: { type: String },
    port: { type: String }
  },
  mongodb: {
    connectionString: { type: String },
    host: { type: String },
    username: { type: String },
    password: { type: String },
    database: { type: String },
    port: { type: String }
  },
  rabbitmq: {
    connectionString: { type: String }
  },

  name: { type: String, default: null },
  description: { type: String, default: null },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('Datasource', schema)
