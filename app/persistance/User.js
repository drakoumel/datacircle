'use strict'
// models/User.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = mongoose.Schema({
  email: { type: String, default: null },
  password: { type: String, default: null },
  salt: { type: String, default: null },
  role: { type: Schema.Types.ObjectId, ref: 'Role' },

  options: { type: Schema.Types.ObjectId, ref: 'UserOptions' },

  verification: {
    key: { type: String, default: null },
    created_at: { type: Date, default: new Date() },
    activated: { type: Boolean, default: false }
  },

  hash: { type: String, default: null },
  token: { type: String, default: null },

  isActive: { type: Boolean, default: false },

  fn: { type: String, default: null },
  ln: { type: String, default: null },
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.statics.findUser = function (email) {
  return this.findOne({ 'email': email })
}

schema.statics.populateCompany = function (id) {
  return this.findOne({ _id: id }).populate('company')
}

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('User', schema)
