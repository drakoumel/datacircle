'use strict'
// models/DecoratedWidget.js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

let schema = new mongoose.Schema({
  widget: { type: Schema.Types.ObjectId, ref: 'Widget' },

  name: { type: String, default: null },
  description: { type: String, default: null },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  createdAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

schema.pre('save', function (next) {
  this.updateAt = new Date()
  next()
})

module.exports = mongoose.model('DecoratedWidget', schema)
