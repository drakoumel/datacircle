// load all the things we need
var LocalStrategy = require('passport-local').Strategy
var FacebookStrategy = require('passport-facebook').Strategy
var TwitterStrategy = require('passport-twitter').Strategy

// hash and salt
var easyPbkdf2 = require('easy-pbkdf2')()

// load up the user model
var User = require('../persistance/User')

// load the auth variables
var configAuth = require('../../config/auth') // use this one for testing

module.exports = function (passport) {
  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id)
  })

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user)
    })
  })

  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  passport.use('local', new LocalStrategy({
    usernameField: 'Email',
    passwordField: 'Password',
    passReqToCallback: true
  },
    function (req, email, password, done) {
      // asynchronous
      process.nextTick(function () {
        User.findOne({
          'email': req.body.Email
        }, function (err, user) {
          if (err) {
            return done(err)
          }
          if (!user) {
            return done(null, false, req.flash('loginMessage', 'No user found.'))
          }
          if (user) {
            easyPbkdf2.verify(user.valid.salt, user.valid.password, password, function (err, valid) {
              if (err) {
                throw err
              }
              if (!valid) {
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'))
              } else {
                return done(null, user)
              }
            })
          } else {
            return done(null, user)
          }
        })
      })
    }))

  // =========================================================================
  // FACEBOOK ================================================================
  // =========================================================================
  passport.use(new FacebookStrategy({
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret,
    callbackURL: configAuth.facebookAuth.callbackURL,
    passReqToCallback: true
  },
    function (req, token, refreshToken, profile, done) {
      process.nextTick(function () {
        if (!req.user) {
          User.findOne({
            'facebook.id': profile.id
          }, function (err, user) {
            if (err) {
              return done(err)
            }
            if (user) {
              // if there is a user id already but no token (user was linked at one point and then removed)
              if (!user.facebook.token) {
                user.facebook.token = token
                user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
                user.facebook.email = profile.emails[0].value

                user.save(function (err) {
                  if (err) {
                    throw err
                  } else {
                    return done(null, user)
                  }
                })
              }

              return done(null, user)
            } else {
              // if there is no user, create them
              var newUser = new User()

              newUser.facebook.id = profile.id
              newUser.facebook.token = token
              newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
              newUser.facebook.email = profile.emails[0].value

              newUser.save(function (err) {
                if (err) {
                  throw err
                }
                return done(null, newUser)
              })
            }
          })
        } else {
          // user already exists and is logged in, we have to link accounts
          var user = req.user // pull the user out of the session

          user.facebook.id = profile.id
          user.facebook.token = token
          user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
          user.facebook.email = profile.emails[0].value

          user.save(function (err) {
            if (err) {
              throw err
            }
            return done(null, user)
          })
        }
      })
    }))

  // =========================================================================
  // TWITTER =================================================================
  // =========================================================================
  passport.use(new TwitterStrategy({
    consumerKey: configAuth.twitterAuth.consumerKey,
    consumerSecret: configAuth.twitterAuth.consumerSecret,
    callbackURL: configAuth.twitterAuth.callbackURL,
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

  },
    function (req, token, tokenSecret, profile, done) {
      // asynchronous
      process.nextTick(function () {
        // check if the user is already logged in
        if (!req.user) {
          User.findOne({
            'twitter.id': profile.id
          }, function (err, user) {
            if (err) {
              return done(err)
            }
            if (user) {
              // if there is a user id already but no token (user was linked at one point and then removed)
              if (!user.twitter.token) {
                user.twitter.token = token
                user.twitter.secretToken = tokenSecret
                user.twitter.username = profile.username
                user.twitter.displayName = profile.displayName

                user.save(function (err) {
                  if (err) {
                    throw err
                  }
                  return done(null, user)
                })
              }
              return done(null, user) // user found, return that user
            } else {
              // if there is no user, create them
              var newUser = new User()

              newUser.twitter.id = profile.id
              newUser.twitter.token = token
              newUser.twitter.secretToken = tokenSecret
              newUser.twitter.username = profile.username
              newUser.twitter.displayName = profile.displayName

              newUser.save(function (err) {
                if (err) {
                  throw err
                }
                return done(null, newUser)
              })
            }
          })
        } else {
          // user already exists and is logged in, we have to link accounts
          var user = req.user // pull the user out of the session

          user.twitter.id = profile.id
          user.twitter.token = token
          user.twitter.secretToken = tokenSecret
          user.twitter.username = profile.username
          user.twitter.displayName = profile.displayName

          user.save(function (err) {
            if (err) {
              throw err
            }
            return done(null, user)
          })
        }
      })
    }))
}
