'use strict'

const AbstractModel = require('./AbstractModel')
const config = require('config')
const chartTypeConfig = config.get('chartType')

class Chart extends AbstractModel {
  constructor () {
    super()
  }

  getChartTypes (callback) {
    let returnConfig = {}
    for (var prop in chartTypeConfig) {
      if (chartTypeConfig.hasOwnProperty(prop)) {
        if (chartTypeConfig[prop].fe) {
          returnConfig[prop] = chartTypeConfig[prop].name
        }
      }
    }

    if (chartTypeConfig) {
      callback({
        data: returnConfig,
        msg: 'acknowledged',
        code: 10
      })
    }
    callback({
      data: null,
      msg: 'value does not exist.',
      code: -10
    })
  }

}

module.exports = new Chart()
