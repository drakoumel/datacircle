'use strict'

const AbstractModel = require('./AbstractModel')
const _DataSource = require('../persistance/Datasource')

const config = require('config')
const dataSourceTypeConfig = config.get('dataSourceType')

const crypto = require('crypto')
const algorithm = 'aes-256-ctr'
const key = 'd6F3Efeq'

class Datasource extends AbstractModel {

  updateDataRef (dataSourceId, dataId) {
    this.findById(dataSourceId, function (ds) {
      ds.dataId = dataId
      ds.save()
    })
  }

  getData (req, callback) {
    let promise = _DataSource.findOne({
      '_id': req.params.dataSourceId,
      'owner': req.user._id
    }).populate('dataId')
    promise
      .then(function (ds) {
        if (ds) {
          ds.dataId = ds.dataId.data
          callback({
            data: ds,
            msg: 'acknowledged',
            code: 'success'
          })
        } else {
          callback({
            data: null,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
      .catch(function (err) {
        console.log(err)
        callback({
          data: null,
          msg: 'acknowledged',
          code: 'error'
        })
      })
  }

  findByName (name, callback) {
    let promise = _DataSource.findOne({
      'name': name
    })
    promise
      .then(function (ds) {
        callback(ds)
      })
      .catch(function (err) {
        console.error(err)
        callback(null)
      })
  }

  findById (id, callback) {
    let promise = _DataSource.findOne({
      '_id': id
    })
    promise
      .then(function (ds) {
        if (ds) {
          callback(ds)
        }
      })
      .catch(function (err) {
        console.error(err)
        callback(null)
      })
  }

  updateOrCreateDataSource (req, callback) {
    var formData = req.body
    formData.user = req.user

    if (formData.type && this.isValidForm(req, formData.type)) {
      _DataSource.findOne({
        'company': req.user.company,
        '_id': formData._id
      })
        .exec((err, dataSource) => {
          if (err) {
            console.log(err)
            callback({
              msg: 'There was an unexpected error!',
              code: 'error'
            })
          } else if (dataSource) {
            this.editDataSource(req, dataSource, callback)
          } else {
            this.registerDataSource(req, callback)
          }
        })
    } else {
      callback({
        msg: 'Invalid Data.',
        code: 'error'
      })
    }
  }

  editDataSource (req, dataSource, callback) {
    var formData = req.body
    formData.user = req.user

    if (formData.type && this.isValidForm(req, formData.type)) {
      let updatedDataSource = this.updateDataSource(formData, dataSource)
      updatedDataSource.save((err) => {
        if (err) {
          callback({
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        }
        callback({
          msg: 'acknowledged',
          code: 'success'
        })
      })
    } else {
      callback({
        msg: 'Invalid Data.',
        code: 'error'
      })
    }
  }

  registerDataSource (req, callback) {
    var formData = req.body
    formData.user = req.user

    if (formData.type && this.isValidForm(req, formData.type)) {
      let dt = this.createDataSource(formData)
      dt.save((err) => {
        if (err) {
          console.log(err)
          callback({
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        }
        callback({
          msg: 'acknowledged',
          code: 'success'
        })
      })
    }
  }

  getDataSourceTypes (req, callback) {
    if (dataSourceTypeConfig) {
      callback({
        data: dataSourceTypeConfig,
        msg: 'acknowledged',
        code: 'success'
      })
    }
    callback({
      data: null,
      msg: 'value does not exist.',
      code: 'error'
    })
  }

  getAvailableDataSources (req, callback) {
    _DataSource.find({
      'owner': req.user._id
    })
      .exec(function (err, dataSource) {
        if (err) {
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (dataSource) {
          callback({
            data: dataSource,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
  }

  /** IS THIS USED? = YES :) **/
  getAvailableDataSourcesEnhanced (req, callback) {
    _DataSource.find({
      'company': req.user.company
    })
      .select('-mysql -mongodb -rabbitmq')
      .exec(function (err, ds) {
        if (err) {
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (ds) {
          callback({
            data: ds,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
  }

  getDataSourceDetails (req, callback) {
    _DataSource.findOne({
      '_id': req.params.dataSourceId
    })
    .exec((err, ds) => {
      if (err) {
        console.log(err)
        callback(null)
      } else if (ds) {
        ds = this.decryptDataSourceData(ds)
        console.log(ds)
        callback(this.reply(ds, 'success', 'success'))
      }
    })
  }

  decryptDataSourceData (datasource) {
    if (Object.getOwnPropertyNames(datasource.mysql.toObject()).length > 0) {
      datasource.mysql.host = this.decrypt(datasource.mysql.host)
      datasource.mysql.username = this.decrypt(datasource.mysql.username)
      datasource.mysql.password = this.decrypt(datasource.mysql.password)
      datasource.mysql.database = this.decrypt(datasource.mysql.database)
      datasource.mysql.port = this.decrypt(datasource.mysql.port.toString())
    }

    if (Object.getOwnPropertyNames(datasource.mongodb.toObject()).length > 0) {
      datasource.mongodb.host = this.decrypt(datasource.mongodb.host)
      datasource.mongodb.username = this.decrypt(datasource.mongodb.username)
      datasource.mongodb.password = this.decrypt(datasource.mongodb.password)
      datasource.mongodb.database = this.decrypt(datasource.mongodb.database)
      datasource.mongodb.port = this.decrypt(datasource.mongodb.port)
    }

    if (Object.getOwnPropertyNames(datasource.rabbitmq.toObject()).length > 0) {
      datasource.rabbitmq.connectionString = this.decrypt(datasource.rabbitmq.connectionString)
    }

    return datasource
  }

  getDataSourceDetailsSecure (req, callback) {
    if (req.params.dataSourceId) {
      _DataSource.findOne({
        'company': req.user.company,
        '_id': req.params.dataSourceId
      })
        .exec((err, dataSource) => {
          if (err) {
            callback({
              data: null,
              msg: 'There was an unexpected error!',
              code: 'error'
            })
          } else if (dataSource) {
            callback({
              data: dataSource,
              msg: 'acknowledged',
              code: 'success'
            })
          }
        })
    } else {
      callback({
        data: null,
        msg: 'No such datasource found',
        code: 'error'
      })
    }
  }

  isValidForm (req, type) {
    req.assert(type, '').notEmpty()

    if (req.validationErrors()) {
      return false
    }

    switch (type) {
      case 'mysql':
      case 'mongodb':
        this.isValidDatabaseForm(req, type)
        break
      case 'rabbitmq':
        this.isValidRabbitMQForm(req, type)
        break
      default:
        return false
    }

    if (req.validationErrors()) {
      return false
    }

    return true
  }

  isValidDatabaseForm (req, type) {
    req.assert('name', '').notEmpty()
    req.assert(type + '.host', '').notEmpty()
    req.assert(type + '.database', '').notEmpty()
    req.assert(type + '.username', '').notEmpty()
    req.assert(type + '.password', '').notEmpty()
    req.assert(type + '.port', '').notEmpty()
  }

  isValidRabbitMQForm (req, type) {
    req.assert(type + '.connectionString', '').notEmpty()
  }

  encrypt (value) {
    let cipher = crypto.createCipher(algorithm, key)
    let crypted = cipher.update(value, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return crypted
  }

  decrypt (value) {
    let decipher = crypto.createDecipher(algorithm, key)
    let dec = decipher.update(value, 'hex', 'utf8')
    dec += decipher.final('utf8')
    return dec
  }

  createDataSource (formData) {
    var ds = new _DataSource()
    ds.type = formData.type
    ds.name = formData.name
    ds.owner = formData.user._id
    ds.company = formData.user.company

    return this.createDataSourceType(formData, ds)
  }

  createDataSourceType (formData, ds) {
    var type = formData.type
    var data = formData[type]

    //ds.schema.paths <-- get all paths
    let schemaArrayPaths = Object.keys(_DataSource.schema.paths)

    schemaArrayPaths.forEach((columnName, index) => {
      if (columnName.indexOf(type) > -1) {
        let strippedcolumnName = columnName.replace(type + '.', '')
        if (data[strippedcolumnName] !== undefined) {
          if (!isNaN(data[strippedcolumnName])) {
            data[strippedcolumnName] = data[strippedcolumnName].toString()
          }
          ds[type][strippedcolumnName] = this.encrypt(data[strippedcolumnName])
        }
      }
    })

    return ds
  }

  updateDataSource (formData, ds) {
    ds.name = formData.name
    ds.type = formData.type

    return this.createDataSourceType(formData, ds)
  }
}

module.exports = new Datasource()
