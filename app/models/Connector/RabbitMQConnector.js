'use strict'

const amqp = require('amqp')

class RabbitMQConnector {
  constructor (ds) {
    return amqp.connect(ds.rabbitmq.connectionString)
  }
}

module.exports = new RabbitMQConnector()
