'use strict'

const mysql = require('mysql')

class MySqlConnector {
  constructor (ds, query) {
    return new Promise(function (resolve, reject) {
      let connection = mysql.createConnection({
        host: ds.mysql.host,
        user: ds.mysql.username,
        password: ds.mysql.password,
        database: ds.mysql.database
      })
      connection.connect()
      connection.query(query, (err, data) => (err ? reject(err) : resolve(data)))
      connection.end((err) => { console.log(err) })
    })
  }
}

module.exports = new MySqlConnector()
