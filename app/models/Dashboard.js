'use strict'

const AbstractModel = require('./AbstractModel')
const _Dashboard = require('../persistance/Dashboard')

class Dashboard extends AbstractModel {

  createOrUpdate (req, callback) {
    _Dashboard
      .findOne({
        _id: req.params.dashboardId,
        company: req.user.company
       })
       .exec()
       .then((dashboard) => {
         if (dashboard) {
           this.update(dashboard, req)
         } else {
          var id = this.create(req)
         }

         callback({
           data: id,
           msg: 'success',
           code: 'success'
         })
       })
       .catch((err) => {
         console.log(err)
         callback({
           data: null,
           msg: err,
           code: 'error'
         })
       })

  }

  getWidgetsForDashboard (req, callback) {
    _Dashboard
      .findOne({ _id: req.params.dashboardId })
      .populate('widgets')
      .exec()
      .then((dash) => {
        if (dash) {
          callback({
            data: dash,
            msg: '',
            code: 'success'
          })
        } else {
          callback({
            data: null,
            msg: '',
            code: 'error'
          })
        }
      })
      .catch((err) => {
        callback({
          data: null,
          msg: err,
          code: 'error'
        })
      })
  }

  removeWidget (req, callback) {
    var promise = _Dashboard.findOne({
      'owner': req.user._id,
      '_id': req.postData.dashboardId
    })

    promise
      .then(function (dashboard) {
        if (dashboard) {
          let index = dashboard.widgets.indexOf(req.postData.widgetId)
          if (index === 0 && dashboard.widgets.length === 1) {
            dashboard.widgets = []
          } else {
            dashboard.widgets.splice(index, 1)
          }
          dashboard.save()
        }

        callback({
          data: null,
          msg: 'The widget has been succesfully removed from the dashboard.',
          code: 'success'
        })
      })
      .catch(function (err, callback) {
        callback({
          data: err,
          msg: 'There was an error removing the widget.',
          code: 'error'
        })
      })
  }

  massRemoveWidget (widgetId, callback) {
    var promise = _Dashboard.find({
      'widgets': {
        '$in': [
          widgetId
        ]
      }
    })

    promise
      .then(function (dashboards) {
        if (dashboards) {
          dashboards.forEach(function (dashboard, index) {
            dashboard.widgets = dashboard.widgets.filter(function (i) {
              return i !== widgetId
            })
            dashboard.save()
          })
        }
      })
      .catch(function (err) {
        return false
      })
  }

  getDashboardsWithWidgets (req, callback) {
    _Dashboard.find({
      'owner': req.user._id
    })
      .populate('widgets')
      .then(function (dashboard) {
        if (dashboard) {
          callback({
            data: dashboard,
            msg: 'acknowledged',
            code: 'success'
          })
        } else {
          callback({
            data: null,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
      .catch(function (err) {
        console.error(err)
        callback({
          data: null,
          msg: 'Unexpected error',
          code: 'error'
        })
      })
  }

  getWidgets (req, callback) {
    var promise = _Dashboard.findOne({
      'owner': req.user._id
    })
    promise
      .then(function (dashboard) {
        if (dashboard) {
          callback({
            data: dashboard,
            msg: 'acknowledged',
            code: 'success'
          })
        } else {
          callback({
            data: null,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
      .catch(function (err) {
        console.error(err)
        callback({
          data: null,
          msg: 'Unexpected error',
          code: 'error'
        })
      })
  }

  addWidget (req, callback) {
    var promise = _Dashboard.findOne({
      '_id': req.postData.dashboardId,
      'owner': req.user._id
    })
    promise
      .then((dashboard) => {
        if (dashboard) {
          if (dashboard.widgets.indexOf(req.postData.widgetId) > -1) {
            callback({
              data: dashboard,
              msg: 'Already Exists',
              code: 'invalid'
            })
          } else {
            dashboard.widgets.push(req.postData.widgetId)
            dashboard.save()

            callback({
              data: dashboard,
              msg: 'acknowledged',
              code: 'success'
            })
          }
        } else {
          this.create(req)
          callback({
            data: null,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
      .catch((err) => {
        console.error(err)
        callback({
          data: null,
          msg: 'Unexpected error',
          code: 'error'
        })
      })
  }

  create (req) {
    var ds = new _Dashboard()
    ds.owner = req.user._id
    if (req.postData.widgetId) {
      ds.widgets.push(req.postData.widgetId)
    }
    ds.company = req.user.company
    ds.description = req.postData.description | ''
    ds.name = req.postData.name
    ds.save()

    return ds._id
  }

  update (dashboard, req) {
    dashboard.name = req.postData.name
    dashboard.description = req.postData.description
    ds.save()
  }

  contains (needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle // eslint-disable-line
    var indexOf

    if (!findNaN && typeof Array.prototype.indexOf === 'function') {
      indexOf = Array.prototype.indexOf
    } else {
      indexOf = function (needle) {
        var i = -1
        var index = -1

        for (i = 0; i < this.length; i++) {
          var item = this[i]

          if ((findNaN && item !== item) || item === needle) { // eslint-disable-line
            index = i
            break
          }
        }

        return index
      }
    }

    return indexOf.call(this, needle) > -1
  }
}

module.exports = new Dashboard()
