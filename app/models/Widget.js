'use strict'

const AbstractModel = require('./AbstractModel')
const _Widget = require('../persistance/Widget')
const _Collective = require('../persistance/Collective')
const _Cache = require('../persistance/Cache')
const DashboardModel = require('../models/Dashboard')

class Widget extends AbstractModel {

  remove (req, callback) {
    var promise = _Widget.findOne({
      'company': req.user.company,
      '_id': req.params.widgetId
    })

    promise
      .then(function (widget) {
        DashboardModel.massRemoveWidget(req.user._id, req.params.widgetId)
        return widget
      })
      .then(function (widget) {
        widget.remove()
      })
      .then(function () {
        callback({
          data: null,
          msg: 'Widget was deleted.',
          code: 'success'
        })
      })
      .catch(function (err) {
        callback({
          data: null,
          msg: 'Could not delete widget.',
          code: 'error'
        })
      })
  }

  updateOrCreateWidget (req, callback) {
    var formData = req.body
    _Widget.findOne({
      'company': req.user.company,
      '_id': formData._id
    })
      .populate('collective')
      .exec((err, widget) => {
        if (err) {
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (widget) {
          this.editWidget(req, callback)
        } else {
          this.registerWidget(req, callback)
        }
      })
  }

  editWidget (req, callback) {
    var formData = req.body
    formData.user = req.user

    this.isWidgetDataValid(req, callback)

    _Widget.findOne({
      'company': req.user.company,
      '_id': formData._id
    })
      .exec((err, widget) => {
        if (err) {
          callback({
            msg: 'There was an unexpected error!' + err,
            code: 'error'
          })
        } else if (widget) {
          widget = this.updateWidget(formData, widget)
          this.deleteCache(widget)
          widget.save(function (err) {
            if (err) {
              callback({
                msg: 'There was an unexpected error!',
                code: 'error'
              })
            }
            callback({
              msg: 'acknowledged',
              code: 'success'
            })
          })
        } else {
          callback({
            msg: 'No such widget found',
            code: 'error'
          })
        }
      })
  }

  registerWidget (req, callback) {
    var formData = req.body

    var newWidget = this.createWidget(formData)
    newWidget.owner = req.user._id
    newWidget.company = req.user.company
    newWidget.save((err) => {
      if (err) {
        callback({
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      }
      callback({
        msg: 'acknowledged',
        code: 'success'
      })
    })
  }

  detailsWidget (req, callback) {
    if (req.params.widgetId) {
      _Widget.findOne({
        'company': req.user.company,
        '_id': req.params.widgetId
      })
        .populate({
          path: 'collectives',
          populate: {path: 'datasource'}
        })
        .then((widget) => {
          if (widget) {
            callback({
              data: widget,
              msg: 'acknowledged',
              code: 'success'
            })
          }
        })
    } else {
      callback({
        data: null,
        msg: 'No such widget found',
        code: 'error'
      })
    }
  }

  availableWidgets (req, callback) {
    _Widget.find({
      'company': req.user.company
    })
      .exec(function (err, widget) {
        if (err) {
          console.log(err)
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (widget) {
          callback({
            data: widget,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
  }

  isWidgetDataValid (req, callback) {
    req.assert('availableDataSource', '').notEmpty()
    req.assert('name', '').notEmpty()

    if (req.validationErrors()) {
      callback({
        msg: 'Data input was not valid!',
        code: 'error'
      })
    }
  }

  updateWidget (formData, widget) {
    widget.name = formData.name
    widget.description = formData.description
    widget.chartType = formData.chartType || 3

    _Collective
    .findOne({ _id: widget.collectives[0] })
    .then((collective) => {
      if (collective) {
        if (formData.query !== undefined) {
          collective.query = formData.query
        } else if (formData.rabbitmq.limits !== undefined && formData.rabbitmq.names !== undefined) {
          collective.rabbitmq.limits = formData.rabbitmq.limits
          collective.rabbitmq.names = formData.rabbitmq.names
        }
        collective.datasource = formData.availableDataSource
        collective.save()
      }
    })

    return widget
  }

  createWidget (formData) {
    let newWidget = new _Widget()
    newWidget.name = formData.name
    newWidget.description = formData.description
    newWidget.widgetType = formData.widgetType

    if (formData.chartType === undefined && formData.type === 'rabbitmq') {
      newWidget.chartType = 5
    } else {
      newWidget.chartType = formData.chartType
    }

    let newCollective = new _Collective()
    newCollective.datasource = formData.availableDataSource
    if (formData.query !== undefined) {
      newCollective.query = formData.query
    } else if (formData.rabbitmq.limits !== undefined && formData.rabbitmq.names !== undefined) {
      newCollective.rabbitmq.limits = formData.rabbitmq.limits
      newCollective.rabbitmq.names = formData.rabbitmq.names
    }

    newCollective.save()

    newWidget.collectives.push(newCollective._id)

    return newWidget
  }

  deleteCache (widget) {
    _Cache.findOneAndRemove({
      '_id': widget.dataId
    }).exec()
  }
}

module.exports = new Widget()
