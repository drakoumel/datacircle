'use strict'

const AbstractModel = require('./AbstractModel')
const Data = require('./Data')
const _Cache = require('../persistance/Cache')
const _Widget = require('../persistance/Widget')

const NO_CACHE_FOUND = 'No cache data found.'
const CACHE_FOUND = 'Cache data found.'

class Cache extends AbstractModel {
  constructor () {
    super()
  }

  get (widgetId, callback) {
    _Cache
    .findOne({ widgetId: widgetId })
    .then((cache) => {
      if (cache) {
        callback(this.reply(cache.data, CACHE_FOUND, this.constants.SUCCESS))
      } else {
        this.populateCache(widgetId, callback)
      }
    })
    .catch((err) => {
      callback(this.reply(null, NO_CACHE_FOUND, this.constants.ERROR, err))
    })
  }

  populateCache (widgetId, callback) {
    _Widget
    .findOne({ _id: widgetId })
    .populate({
      path: 'collectives',
      populate: {path: 'datasource'}
    })
    .then((widget) => {
      return Data.pool(widget)
    })
    .then((data) => {
      if (data) {
        this.saveCacheData(widgetId, data)
        callback(this.reply(data, 'got data', 'success'))
      } else {
        callback(this.reply(null, 'Could not retrieve data.', 'error'))
      }
    })
    .catch((err) => {
      console.log(err)
      callback(this.reply(null, 'error', this.constants.ERROR, err))
    })
  }

  saveCacheData (widgetId, data, callback) {
    let newCache = new _Cache()
    newCache.widgetId = widgetId
    newCache.data = data
    newCache.save()
  }
}

module.exports = new Cache()
