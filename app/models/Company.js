'use strict'

const AbstractModel = require('./AbstractModel')
const _Company = require('../persistance/Company')
const assert = require('assert')
const NO_COMAPNY_FOUND = 'No cache data found.'
const COMPANY_FOUND = 'Cache data found.'
const COMPANY_SAVED = 'Cache saved.'
const COMPANY_NAME_EXISTS = 'Comapny has already been registered'

class Company extends AbstractModel {
  constructor () {
    super()
  }

  findOneOrCreate (companyName, ownerId) {
    return _Company.findCompany(companyName).exec().then(function (company) {
      if (company) {
        return company
      } else {
        let newCompany = new _Company()
        newCompany.name = companyName
        newCompany.description = ''
        return newCompany.save()
      }
    })
  }

  addMember(name, memberId, callback) {
    let promise = _Company.getCompany(name)

    promise
      .then((company) => {
        if (company) {
          company.members.push(memberId)
          company.save((err) => {
            if (err) {
              callback(this.reply(null, NO_COMAPNY_FOUND, this.constants.ERROR))
            } else {
              callback(this.reply(company, COMPANY_SAVED, this.constants.SUCCESS))
            }
          })
        } else {
          callback(this.reply(null, NO_COMAPNY_FOUND, this.constants.ERROR))
        }
      })
      .catch((err) => {
        callback(this.reply(null, NO_COMAPNY_FOUND, this.constants.ERROR, err))
      })
  }

  getCompany(name, callback) {
    let promise = _Company.getCompany(name)

    promise
      .then((company) => {
        if (company) {
          callback(this.reply(company, COMPANY_FOUND, this.constants.SUCCESS))
        } else {
          callback(this.reply(null, NO_COMAPNY_FOUND, this.constants.ERROR))
        }
      })
      .catch((err) => {
        callback(this.reply(null, NO_COMAPNY_FOUND, this.constants.ERROR, err))
      })
  }
}

module.exports = new Company()
