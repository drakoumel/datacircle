'use strict'

const AbstractModel = require('./AbstractModel')
const _Role = require('../persistance/Role')

const NO_ROLE_FOUND = 'No role found.'
const ROLE_FOUND = 'Role found.'
const ROLE_SAVED = 'Role saved.'
const ROLE_SAVE_FAIL = 'Could not save role.'

class Role extends AbstractModel {
  constructor () {
    super()
  }

  getEmptyRoles () {
    return ['admin',
      'widgetRead',
      'widgetWrite',
      'datasourceRead',
      'datasourceWrite',
      'dashboardRead',
      'dashboardWrite'
    ]
  }

  getRoles (user, callback) {
    if (user && user.role.access.admin) {
      let promise = _Role.find({ 'company': user.fkcompany })
      promise
        .then((roles) => {
          callback(this.reply(roles, ROLE_FOUND, this.constants.SUCCESS))
        })
        .catch((err) => {
          callback(this.reply(null, NO_ROLE_FOUND, this.constants.ERROR, err))
        })
    } else {
      callback(this.reply(null, NO_ROLE_FOUND, this.constants.ERROR, this.constants.NO_PRIVILEDGE))
    }
  }

  createOrUpdateRole (req, callback) {
    _Role
    .findOne({ _id: req.body._id })
    .exec()
    .then((role) => {
      if (role) {
        this.setRoleData(role, req.body).save()
      } else {
        this.setRoleData(new _Role(), req.body).save()
      }
      callback(this.reply(role, ROLE_SAVED, this.constants.SUCCESS))
    })
    .catch((err) => {
      callback(this.reply(null, ROLE_SAVE_FAIL, this.constants.ERROR, err))
    })
  }

  findOneOrCreateAdminRole (ownerId, companyId) {
    return _Role.findRole(companyId, 'Administrator').exec()
    .then(function (role) {
      if (role) {
        return role
      } else {
        var newRole = new _Role()
        newRole.fkcompany = companyId
        newRole.owner = ownerId
        newRole.name = 'Administrator'
        newRole.access.admin = true
        return newRole.save()
      }
    })
  }

  setRoleData (role, body) {
    role.name = body.name
    role.access.admin = body.admin || false
    role.access.widgetRead = body.widgetRead || false
    role.access.widgetWrite = body.widgetWrite || false
    role.access.datasourceRead = body.datasourceRead || false
    role.access.datasourceWrite = body.datasourceWrite || false
    role.access.dashboardRead = body.dashboardRead || false
    role.access.dashboardWrite = body.dashboardWrite || false
    return role
  }
}

module.exports = new Role()
