var DataNormalizerMySQL = require('./DataNormalizer/MySQL.js')
var DataNormalizerMongoDB = require('./DataNormalizer/MongoDB.js')
var DataSource = require('../models/DataSource')
var DataModel = require('../models/Data')
var mysql = require('mysql')
var MongoClient = require('mongodb').MongoClient

class Data {
  constructor () {}

  dataInput (req, callback) {
    var formData = {
      userToken: req.postData.token,
      ds_id: req.postData.ds_id,
      type: req.postData.type,
      data: req.postData.data
    }

    if ((typeof formData.userToken === 'undefined' || formData.userToken == null) ||
      (typeof formData.data === 'undefined' || formData.data == null)) {
      callback({
        msg: 'Please provide all required fields (token,data,add)',
        code: -1
      })
    }

    if (typeof formData.type === 'undefined' || formData.type == null) {
      formData.type = 1 // default Raw
    } else if (formData.type !== 1) {
      // return { msg: "We currently only support Raw Type data input via post.", code: 2}
    }

    if (typeof formData.createWidget === 'undefined' || formData.createWidget == null) {
      formData.createWidget = false
    }
    if (typeof formData.name === 'undefined' || formData.name == null) {
      formData.name = 'create Random Name here'
    }

    if (formData.data !== 'undefined' && formData.data != null && formData.data.length > 0) {
      var data = new DataModel()
      data.data = formData.data
      data.save(function () {
        DataSource.updateDataRef(formData.ds_id, data._id)
      })

      callback({
        msg: 'acknowledged',
        code: 'success'
      })
    } else {
      callback({
        msg: 'empty data',
        code: 'error'
      })
    }
  }

  pool (widgetFull) {
    let promise
    widgetFull.collectives.forEach((collective, key) => {
      switch (collective.datasource.type) {
        case 'mysql':
          collective.datasource.mysql.host = DataSource.decrypt(collective.datasource.mysql.host)
          collective.datasource.mysql.username = DataSource.decrypt(collective.datasource.mysql.username)
          collective.datasource.mysql.password = DataSource.decrypt(collective.datasource.mysql.password)
          collective.datasource.mysql.database = DataSource.decrypt(collective.datasource.mysql.database)
          collective.datasource.mysql.port = DataSource.decrypt(collective.datasource.mysql.port)
          promise = this.poolMysql(collective, widgetFull)
          break
        case 'mongodb':
          collective.datasource.mongodb.host = DataSource.decrypt(collective.datasource.mongodb.host)
          collective.datasource.mongodb.username = DataSource.decrypt(collective.datasource.mongodb.username)
          collective.datasource.mongodb.password = DataSource.decrypt(collective.datasource.mongodb.password)
          collective.datasource.mongodb.database = DataSource.decrypt(collective.datasource.mongodb.database)
          collective.datasource.mongodb.port = DataSource.decrypt(collective.datasource.mongodb.port)
          promise = this.poolMongoDb(collective)
          break
        case 'rabbitmq':
          collective.datasource.rabbitmq.connectionString = DataSource.decrypt(collective.datasource.rabbitmq.connectionString)
          promise = this.poolRabbitMq(collective, widgetFull)
          break
        default:
          this.poolMysql(collective, widgetFull)
          break
      }
    })

    return promise
  }

  poolRabbitMq (collective, widgetFull) {
    let ds = collective.datasource
    let names = collective.rabbitmq.names.split(',')
    let limits = collective.rabbitmq.limits.split(',')

    let connection = require('amqplib').connect(ds.rabbitmq.connectionString)
    let data = []

    return connection.then(function (conn) {
      return conn.createChannel()
    }).then((ch) => {
      if (ch) {
        let queueAsserts = []
        names.forEach((name, index) => {
          queueAsserts.push(
            ch.assertQueue(name)
            .then((q) => {
              if (q) {
                let innerData = []
                innerData.push({ label: name + ' Message Counter', value: q.messageCount })
                innerData.push({ label: name + ' Limit Counter', value: limits[index] })
                data.push(innerData)
              }
            })
          )
        })
        return Promise.all(queueAsserts)
        .then(() => {
          return data
        })
      } else {
        return data
      }
    })
  }

  poolMysql (collective, widgetFull) {
    let ds = collective.datasource
    let query = collective.query

    return new Promise(function (resolve, reject) {
      let connection = mysql.createConnection({
        host: ds.mysql.host,
        user: ds.mysql.username,
        password: ds.mysql.password,
        database: ds.mysql.database
      })
      connection.connect()
      connection.query(query, (err, data) => (err ? reject(err) : resolve(data)))
      connection.end((err) => {
        console.log(err)
      })
    })
      .then((rows) => {
        return DataNormalizerMySQL.process(widgetFull.chartType, rows)
      })
  }

  poolMongoDb (enhancedWidgetModel, callback) {
    //  mongoose.connect('mongodb://username:password@host:port/database?options...')
    var uri = 'mongodb://' +
      enhancedWidgetModel.datasource.mongodb.username + ':' +
      enhancedWidgetModel.datasource.mongodb.password + '@' +
      enhancedWidgetModel.datasource.mongodb.host + ':' +
      enhancedWidgetModel.datasource.mongodb.port + '/' +
      enhancedWidgetModel.datasource.mongodb.database + '' +
      '?authMechanism=SCRAM-SHA-1'

    uri = 'mongodb://heroku_md4x6qzn:ovm4ebjt4jlsrosfj5p71beuji@ds019143.mlab.com:19143/heroku_md4x6qzn?authMechanism=SCRAM-SHA-1'

    return MongoClient.connect(uri)
    .then((db, err) => {
      if (err) {
        console.log(err)
        console.log(1)
        return null
      }

      var container = {}

      container.query = enhancedWidgetModel.query.trim()
      container.collectionName = container.query.substr(0, container.query.indexOf('.'))
      container.collection = db.collection(container.collectionName)
      container.action = container.query.substring(container.query.indexOf('.') + 1, container.query.indexOf('('))

      var queryStartIndex = container.query.indexOf('(') + 1
      var queryEndIndex = container.query.length - 1
      if (container.action === 'aggregate' && container.query.indexOf('[') > 0) {
        queryStartIndex = container.query.indexOf('[') + 1
        queryEndIndex = container.query.lastIndexOf('}') + 1
      }

      container.query = container.query.substring(queryStartIndex, queryEndIndex)
      container.query = container.query.replace(new RegExp(' ', 'g'), '')

      var strs = container.query.split('},')
      var jsonArray = []

      try {
        strs.forEach(function (item, index) {
          if (index < strs.length - 1) {
            item += '}'
          }
          jsonArray.push(JSON.parse(item))
        })
      } catch (ex) {
        console.log(ex)
        console.log(2)
        return null
      }

      if (container.action === 'aggregate') {
        container.query = jsonArray
      } else {
        container.query = jsonArray[0]
      }

      var query = (container.collection)[container.action](container.query)

      var afterFunction = 'toArray'
      if (container.action === 'findOne') {
        afterFunction = 'then'
      }

      try {
        query[afterFunction](function (err, data) {
          if (err) {
            console.log(err)
            console.log(3)
            return null
          } else if (data && data.length !== 0) {
            let dataModified = DataNormalizerMongoDB.process(enhancedWidgetModel.chartType, data)
            return dataModified
          } else {
            return null
          }
          db.close()
        })
      } catch (ex) {
        console.log(ex)
        console.log(4)
        db.close()
        return null
      }
    })
  }
}

module.exports = new Data()
