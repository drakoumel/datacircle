'use strict'

const AbstractModel = require('./AbstractModel')

class Heartbeat extends AbstractModel {
  constructor () {
    super()
  }

  test (callback) {
    callback(this.reply(null, 'acknowledged', this.constants.SUCCESS))
  }
}

module.exports = new Heartbeat()
