'use strict'

const config = require('config')
const mailTemplates = config.get('mailTemplates')
const mailgunConfig = config.get('mailgun')
const mailgun = require('mailgun-js')({
  apiKey: mailgunConfig.apiKey,
  domain: mailgunConfig.domain
})

/**
 * type = mailTemplates
 * data = !recepient, company, hash
 */
exports.send = function (type, data) {
  let mailConfig = mailTemplates[type]
  let context = mailConfig.html

  let modifiedContext = getMailTemplateContext(type, context, data)

  let newData = {
    from: 'DataCircle <noreply@datacircle.io>',
    to: data.recepient,
    subject: mailConfig.subject,
    html: modifiedContext
  }

  mailgun.messages().send(newData, function (err, body) {
    if (err) {
      console.log(err)
    }
  })
}

function getMailTemplateContext (type, context, data) {
  var modifiedContext = context
  modifiedContext = modifiedContext.replace('$url', config.url)

  switch (type) {
    case 'invitation':
      modifiedContext = modifiedContext
        .replace('$userHash', data.userHash)
        .replace('$company', data.company)
      break
    case 'accountVerification':
      modifiedContext = modifiedContext
        .replace('$userVericifationKey', data.verificationKey)
      break
  }

  return modifiedContext
}
