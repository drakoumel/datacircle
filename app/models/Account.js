'use strict'

const AbstractModel = require('./AbstractModel')

let easyPbkdf2 = require('easy-pbkdf2')()
let _User = require('../persistance/User')
let _Company = require('../persistance/Company')
let _Role = require('./Role')
let _UserOptions = require('../persistance/UserOptions')
let Mail = require('./Mail')
let crypto = require('crypto')
let base64url = require('base64url')

const FOUND_USER = 'User Found.'
const USER_NOT_FOUND = 'User not Found.'
const USER_INVITE_FAIL = 'Could not invite User.'
const USER_INVITE_SUCCESS = 'User invited!'
const VERIFICATION_FAIL = 'Verification Failed!'
const VERIFICATION_SUCESS = 'Verification Success!'
const USER_SAVE_SUCCESS = 'User saved.'
const USER_SAVE_FAIL = 'User could not be saved.'
const USER_LOGIN_FAIL = 'No such email/password combination was found'
const USER_LOGIN_SUCCESS = 'Login succesful'

class Account extends AbstractModel {
  constructor () {
    super()
  }

  findUserByToken (token, callback) {
    let promise = _User.findOne({ 'token': token })
    promise
      .then((user) => {
        callback(this.reply(user, FOUND_USER, this.constants.SUCCESS))
      })
      .catch((err) => {
        callback(this.reply(null, USER_NOT_FOUND, this.constants.ERROR, err))
      })
  }

  findUserByEmail (email, callback) {
    let promise = _User.findOne({ 'email': email })
    promise
      .then((user) => {
        callback(this.reply(user, FOUND_USER, this.constants.SUCCESS))
      })
      .catch((err) => {
        callback(this.reply(null, USER_NOT_FOUND, this.constants.ERROR, err))
      })
  }

  userInvite (email, companyId, roleId, callback) {
    let newUser = new _User()
    newUser.hash = base64url(crypto.randomBytes(20))

    _Company
    .findOne({ _id: companyId })
    .exec()
    .then((company) => {
      if (company) {
        newUser.company = companyId
        newUser.role = roleId
        newUser.save((err) => {
          if (err) {
            callback(this.reply('/', USER_INVITE_FAIL, this.constants.ERROR, err))
          } else {
            var data = {
              'recepient': email,
              'userHash': newUser.hash,
              'company': company.name
            }
            Mail.send('invitation', data)
            callback(this.reply(null, USER_INVITE_SUCCESS, this.constants.SUCCESS))
          }
        })
      } else {
        callback(this.reply('/', USER_INVITE_FAIL, this.constants.ERROR))
      }
    })
  }

  verify (key, callback) {
    _User.findOne({
      'verification.key': key
    })
      .populate('role')
      .exec((err, user) => {
        if (err) {
          callback(this.reply('/', VERIFICATION_FAIL, this.constants.ERROR, err))
        } else if (user) {
          user.verification.key = ''
          user.verification.activated = true
          user.save(function (user, err) {
            if (err) {
              callback(this.reply(null, USER_SAVE_FAIL, this.constants.ERROR, err))
            } else {
              callback(this.reply(user, VERIFICATION_SUCESS, this.constants.SUCCESS))
            }
          })
        } else {
          callback(super.reply('/', VERIFICATION_FAIL, this.constants.ERROR, err))
        }
      })
  }

  login (body, callback) {
    _User.findOne({ 'email': body.Email })
    .populate('role')
    .exec()
    .then((user) => {
      if (!user) {
        throw new Error(USER_LOGIN_FAIL)
      } else {
        easyPbkdf2.verify(user.salt, user.password, body.Password, (err, valid) => {
          if (!valid) {
            callback(this.reply('/#login', USER_LOGIN_FAIL, this.constants.ERROR, err))
          } else {
            user.salt = null
            user.password = null
            callback({
              user: user,
              url: '/',
              msg: USER_LOGIN_SUCCESS,
              code: this.constants.SUCCESS
            })
          }
        })
      }
    })
    .catch((err) => {
      callback(this.reply('/#login', USER_LOGIN_FAIL, this.constants.ERROR, err))
    })
  }

  createEmptyOptions () {
    let newOptions = new _UserOptions()
    newOptions.save()
    return newOptions._id
  }

  createNewUser (body, company) {
    var newUser = new _User()
    newUser.email = body.Email
    newUser.isActive = true
    newUser.ln = body.LN
    newUser.fn = body.FN
    newUser.company = company._id
    newUser.verification.key = this._randomStringAsBase64Url(20)
    newUser.token = this._randomStringAsBase64Url(20)
    newUser.password = body.Password
    newUser.options = this.createEmptyOptions()

    return _Role.findOneOrCreateAdminRole(newUser._id, newUser.company)
    .then((role) => {
      newUser.role = role._id
      return newUser
    })
    .then((newUser) => {
      this._hashPassword(newUser)
      return newUser.save()
    })
  }

  updateSelectedDashbaord (dashboardId, user, callback) {
    _UserOptions.findOne({ _id: user.options._id })
    .then((userOptions) => {
      if (userOptions) {
        userOptions.selectedDashboard = dashboardId
        userOptions.save()
        callback({
          data: null,
          msg: 'success1',
          code: this.constants.SUCCESS
        })
      } else {
        callback({
          data: null,
          msg: 'success2',
          code: this.constants.SUCCESS
        })
      }
    })
    .catch((err) => {
      console.log(err)
      callback({
        data: null,
        msg: 'error',
        code: this.constants.ERROR
      })
    })
  }

  _hashPassword (newUser) {
    easyPbkdf2.secureHash(newUser.password, easyPbkdf2.generateSalt(),
      function (err, passwordHash, originalSalt) {
        newUser.salt = originalSalt
        newUser.password = passwordHash
        newUser.save()
      })
  }

  isRePasswordValid (req, callback) {
    return (req.body.Password === req.body.RePassword)
  }

  _registerInvitedUser (req, callback) {
    if (!req.user && this._isCompleteRegistrationDataValid(req)) {
      this.validatePassword(req, callback)
    }
  }

  _isLoginDataValid (req) {
    req.assert('Email', '').notEmpty()
    req.assert('Password', '').notEmpty()
    if (req.validationErrors()) {
      return false
    }
    return true
  }

  _validatePassword (req, callback) {
    if (req.body.Password !== req.body.RePassword) {
      callback({
        url: '/#register',
        msg: 'Passwords do not match',
        code: 'error'
      })
    }
  }

  _randomStringAsBase64Url (size) {
    return base64url(crypto.randomBytes(size))
  }

  isRegisterDataValid (req, callback) {
    req.assert('Email', '').notEmpty()
    req.assert('Password', '').notEmpty()
    req.assert('RePassword', '').notEmpty()
    req.assert('FN', '').notEmpty()
    req.assert('LN', '').notEmpty()
    req.assert('Company', '').notEmpty()

    if (req.validationErrors()) {
      return false
    }
    return true
  }

  _isCompleteRegistrationDataValid (req, callback) {
    req.assert('Email', '').notEmpty()
    req.assert('Password', '').notEmpty()
    req.assert('RePassword', '').notEmpty()
    req.assert('FN', '').notEmpty()
    req.assert('LN', '').notEmpty()
    req.assert('Hash', '').notEmpty()

    if (req.validationErrors()) {
      return false
    }
    return true
  }
}

module.exports = new Account()
