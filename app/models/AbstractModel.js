'use strict'

const _Constants = require('../system/constants')

class AbstractModel {
  constructor () {
    this.constants = _Constants
  }

  reply (data, msg, code, err) {
    let returnCode = code
    if (code === this.constants.ERROR) {
      returnCode = 'error'
    } else {
      returnCode = 'success'
    }

    return {
      data: data,
      msg: msg,
      code: returnCode
    }
  }
}

module.exports = AbstractModel
