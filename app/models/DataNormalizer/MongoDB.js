exports.process = function (chartType, data) {
  switch (chartType) {
    case 1:
      data = lineBar(data)
      break
    case 2:
      data = lineBar(data)
      break
    case 3:
      data = pieDonut(data)
      break
    case 4:
      data = pieDonut(data)
      break
    default:
      data = lineBar(data)
  }

  return data
}

function pieDonut (data) {
  var valArray = []
  data.forEach(function (element) {
    valArray.push({label: element[Object.keys(element)[0]], value: element[Object.keys(element)[1]]})
  })

  data = {
    'data': valArray
  }

  return data
}

function lineBar (data) {
  var labels = []
  var yArray = []

  for (var j = 1; j <= Object.keys(data[0]).length - 1; j++) {
    yArray[j] = []
  }

  data.forEach(function (element) {
    labels.push(element[Object.keys(element)[0]])
    for (var i = 1; i <= Object.keys(element).length - 1; i++) {
      yArray[i].push(element[Object.keys(element)[i]])
    }
  })

  data = {
    'x': labels,
    'y': yArray
  }

  return data
}
