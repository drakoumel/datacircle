'use strict'

const Company = require('../persistance/Company')
const User = require('../persistance/User')
const assert = require('assert')
const _CompanyModel = require('../models/Company')
const _AccountModel = require('../models/Account')
const _MailModel = require('../models/Mail')

class Account {
  constructor () {
  }

  createNewUser (body, callback) {
    User.findUser(body.Email).exec()
    .then((user) => {
      if (user) {
        throw new Error('user already exists')
      } else {
        return Company.findCompany(body.Company).exec()
      }
    })
    .then(function (company) {
      if (company) {
        throw new Error('Company already exists')
      } else {
        return _CompanyModel.findOneOrCreate(body.Company)
      }
    })
    .then((newCompany) => {
      if (!newCompany) {
        throw new Error('failed to create company')
      }
      return _AccountModel.createNewUser(body, newCompany)
    })
    .then((newUser) => {
      _MailModel.send('accountVerification', {
        'recepient': newUser.email,
        'verificationKey': newUser.verification.key
      })
      return newUser
    })
    .then((newUser) => {
      callback({
        user: newUser,
        url: '/',
        msg: 'You have been registered successfuly!',
        code: 'success'
      })
      return
    })
    .catch((err) => {
      callback({
        user: null,
        url: '/#register',
        msg: err.message,
        code: 'error'
      })
    })
  }
}

module.exports = new Account()
