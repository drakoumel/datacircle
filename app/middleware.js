'use strict'

let AccountHandler = require('./models/Account')
var DataSource = require('./modules-old/Data-source')
var User = require('./persistance/User')

module.exports = function (router, env) {
  // default, flash msging and session to user passing
  router.use(function (req, res, next) { flashMessage(req, res); next() })
  router.use(function (req, res, next) { sessionAuth(req, res, next) })

  // body || request the same
  router.use(function (req, res, next) {
    getParams(req, res, next)
  })

  // for data posts only
  router.use('/data', function (req, res, next) {
    confirmDataSource(req.body.datasource, function (ds) {
      if (ds) {
        req.body.ds_id = ds._id
        next()
      } else {
        next(new Error('Not a valid datasource'))
      }
    })
  })

  router.use('/data', function (req, res, next) {
    authToken(req.body.token, function (user) {
      if (user) {
        next()
      } else {
        next(new Error('Not a valid token'))
      }
    })
  })
}

function sessionAuth (req, res, next) {
  if (req.session && req.session.user) {
    User.findOne({
      'email': req.session.user.email
    })
      .populate('role')
      .populate('options')
      .exec(function (err, user) {
        if (err) {
          console.log(err)
        }
        if (user) {
          req.user = user
          req.user.password = null
          req.user.salt = null
          req.user.token = null
          req.user.verification = null
          req.session.user = user
          res.locals.user = user
          next()
        } else {
          next()
        }
      })
  } else {
    next()
  }
}

function getParams (req, res, next) {
  if (typeof req.body !== 'undefined' || req.body.keys({}).length > 0 || req.body != null) {
    var postData = req.body
  }

  if (req.query && postData == null) {
    postData = req.query
  }

  req.postData = postData
  next()
}

function flashMessage (req, res) {
  res.locals.success_messages = req.flash('success')
  res.locals.error_messages = req.flash('error')
}

function confirmDataSource (datasourceName, callback) {
  DataSource.findByName(datasourceName, function (ds) {
    callback(ds)
  })
}

function authToken (token, callback) {
  AccountHandler.findUserByToken(token, function (user) {
    callback(user)
  })
}
