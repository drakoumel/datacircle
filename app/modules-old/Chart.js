// modules/Chart.js
var config = require('config')
var chartTypeConfig = config.get('chartType')

exports.getChartTypes = function (req, callback) {
  if (chartTypeConfig) {
    callback({
      data: chartTypeConfig,
      msg: 'acknowledged',
      code: 10
    })
  }
  callback({
    data: null,
    msg: 'value does not exist.',
    code: -10
  })
}
