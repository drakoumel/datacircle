var RoleModel = require('../models/Role')

exports.getRoles = function (req, callback) {
  if (req.user && req.user.role.access.admin) {
    var promise = RoleModel.find({ 'company': req.user.metadata.company })
    promise
      .then(function (roles) {
        callback({
          data: roles,
          msg: 'acknowledged',
          code: 'success'
        })
      })
      .catch(function (err) {
        console.log(err)
        callback({
          data: null,
          msg: 'Unexpected error',
          code: 'error'
        })
      })
  } else {
    callback({
      msg: 'unathorized',
      code: 'error'
    })
  }
}

exports.getEmptyRole = function () {
  return ['admin',
    'widgetRead',
    'widgetWrite',
    'datasourceRead',
    'datasourceWrite',
    'dashboardRead',
    'dashboardWrite'
  ]
}

exports.createDecoratedRole = function (req, callback) {
  var promise = RoleModel.findOne({
    'owner': req.user._id,
    'company': req.user.metadata.company,
    'metadata.name': req.body.name,
    'access.admin': req.body.admin,
    'access.widgetRead': req.body.widgetRead,
    'access.widgetWrite': req.body.widgetWrite,
    'access.datasourceRead': req.body.datasourceRead,
    'access.datasourceWrite': req.body.datasourceWrite,
    'access.dashboardRead': req.body.dashboardRead,
    'access.dashboardWrite': req.body.dashboardWrite
  })

  promise
    .then(function (role) {
      if (role) {
        callback({
          msg: 'acknowledged',
          code: 'success'
        })
      } else {
        createRole(req, callback)
      }
    })
    .catch(function (err) {
      console.log(err)
      callback({
        data: null,
        msg: 'Unexpected error',
        code: 'error'
      })
    })
}

function createRole (req, callback) {
  var newRole = new RoleModel()
  newRole.owner = req.user._id
  newRole.company = req.user.metadata.company
  newRole.metadata.name = req.body.name
  newRole.access.admin = req.body.admin || false
  newRole.access.widgetRead = req.body.widgetRead || false
  newRole.access.widgetWrite = req.body.widgetWrite || false
  newRole.access.datasourceRead = req.body.datasourceRead || false
  newRole.access.datasourceWrite = req.body.datasourceWrite || false
  newRole.access.dashboardRead = req.body.dashboardRead || false
  newRole.access.dashboardWrite = req.body.dashboardWrite || false

  newRole.save(function (err) {
    if (err) {
      console.log(err)
      callback({
        data: null,
        msg: 'Unexpected error',
        code: 'error'
      })
    } else {
      callback({
        role: newRole,
        msg: 'Role Created.',
        code: 'success'
      })
    }
  })
}

exports.getAdminRoleDummy = function (userId, company) {
  var newRole = new RoleModel()
  newRole.company = company
  newRole.owner = userId
  newRole.metadata.name = 'admin'
  newRole.access.admin = true
  newRole.save()
  return newRole
}

exports.createAdminRole = function (user, callback) {
  var promise = RoleModel.findOne({
    'owner': user._id,
    'company': user.metadata.company,
    'metadata.name': 'admin',
    'access.admin': true
  })
  promise
    .then(function (role) {
      if (role) {
        callback({
          msg: 'acknowledged',
          code: 'success'
        })
      } else {
        createRole(req, callback)
      }
    })
    .catch(function (err) {
      console.log(err)
      callback({
        data: null,
        msg: 'Unexpected error',
        code: 'error'
      })
    })
}
