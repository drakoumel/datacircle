var config = require('config')
var dataSourceTypeConfig = config.get('dataSourceType')
var DataSource = require('../models/DataSource')

module.exports = {
  updateOrCreateDataSource: updateOrCreateDataSource,
  getAvailableDataSources: getAvailableDataSources,
  getDataSourceTypes: getDataSourceTypes,
  getDataSourceDetails: getDataSourceDetails,
  findByName: findByName,
  getData: getData,
  updateDataRef: updateDataRef,
  getAvailableDataSourcesEnhanced: getAvailableDataSourcesEnhanced
}

function updateDataRef (dataSourceId, dataId) {
  findById(dataSourceId, function (ds) {
    ds.dataId = dataId
    ds.save()
  })
}

function getData (req, callback) {
  var promise = DataSource.findOne({
    '_id': req.params.dataSourceId,
    'owner': req.user._id
  }).populate('dataId')
  promise
    .then(function (ds) {
      if (ds) {
        ds.dataId = ds.dataId.data
        callback({
          data: ds,
          msg: 'acknowledged',
          code: 'success'
        })
      } else {
        callback({
          data: null,
          msg: 'acknowledged',
          code: 'success'
        })
      }
    })
    .catch(function (err) {
      console.log(err)
      callback({
        data: null,
        msg: 'acknowledged',
        code: 'error'
      })
    })
}

function findByName (name, callback) {
  var promise = DataSource.findOne({
    'name': name
  })
  promise
    .then(function (ds) {
      callback(ds)
    })
    .catch(function (err) {
      console.error(err)
      callback(null)
    })
}

function findById (id, callback) {
  var promise = DataSource.findOne({
    '_id': id
  })
  promise
    .then(function (ds) {
      if (ds) {
        callback(ds)
      }
    })
    .catch(function (err) {
      console.error(err)
      callback(null)
    })
}

function updateOrCreateDataSource (req, callback) {
  var formData = req.body
  formData.user = req.user

  if (formData.type && isValid(req, formData.type)) {
    DataSource.findOne({
      'company': req.user.metadata.company,
      '_id': formData._id
    })
      .exec(function (err, dataSource) {
        if (err) {
          callback({
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (dataSource) {
          editDataSource(req, dataSource, callback)
        } else {
          registerDataSource(req, callback)
        }
      })
  } else {
    callback({
      msg: 'Invalid Data.',
      code: 'error'
    })
  }
}

function editDataSource (req, dataSource, callback) {
  var formData = req.body
  formData.user = req.user

  if (formData.type && isValid(req, formData.type)) {
    dataSource = updateDataSource(formData, dataSource)
    dataSource.save(function (err) {
      if (err) {
        callback({
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      }
      callback({
        msg: 'acknowledged',
        code: 'success'
      })
    })
  } else {
    callback({
      msg: 'Invalid Data.',
      code: 'error'
    })
  }
}

function registerDataSource (req, callback) {
  var formData = req.body
  formData.user = req.user

  if (formData.type && isValid(req, formData.type)) {
    var dt = createDataSource(formData)
    dt.save(function (err) {
      if (err) {
        callback({
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      }
      callback({
        msg: 'acknowledged',
        code: 'success'
      })
    })
  }
}

function getDataSourceTypes (req, callback) {
  if (dataSourceTypeConfig) {
    callback({
      data: dataSourceTypeConfig,
      msg: 'acknowledged',
      code: 'success'
    })
  }
  callback({
    data: null,
    msg: 'value does not exist.',
    code: 'error'
  })
}

function getAvailableDataSources (req, callback) {
  process.nextTick(function () {
    DataSource.find({
      'owner': req.user._id
    })
      .exec(function (err, dataSource) {
        if (err) {
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (dataSource) {
          callback({
            data: dataSource,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
  })
}

function getAvailableDataSourcesEnhanced (req, callback) {
  DataSource.find({
    'company': req.user.metadata.company
  })
    .select('-mysql -mongodb')
    .exec(function (err, ds) {
      if (err) {
        console.error(err)
        callback({
          data: null,
          msg: '1 There was an unexpected error!',
          code: 'error'
        })
      } else if (ds) {
        callback({
          data: ds,
          msg: 'acknowledged',
          code: 'success'
        })
      }
    })
}

function getDataSourceDetails (req, callback) {
  if (req.params.dataSourceId) {
    DataSource.findOne({
      'company': req.user.metadata.company,
      '_id': req.params.dataSourceId
    })
      .exec(function (err, dataSource) {
        if (err) {
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (dataSource) {
          callback({
            data: dataSource,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
  } else {
    callback({
      data: null,
      msg: 'No such datasource found',
      code: 'error'
    })
  }
}

function isValid (req, type) {
  req.assert('name', '').notEmpty()

  req.assert(type + '.host', '').notEmpty()
  req.assert(type + '.database', '').notEmpty()
  req.assert(type + '.username', '').notEmpty()
  req.assert(type + '.password', '').notEmpty()

  if (req.validationErrors()) {
    return false
  }
  return true
}

function createDataSource (formData) {
  var dt = new DataSource()
  dt.type = formData.type
  dt.name = formData.name
  dt.owner = formData.user._id
  dt.company = formData.user.metadata.company

  dt = createDataSourceType(formData, dt)

  return dt
}

function createDataSourceType (formData, dt) {
  var type = formData.type
  var data = formData[type]

  dt[type].host = data.host
  dt[type].database = data.database
  dt[type].username = data.username
  dt[type].password = data.password
  dt[type].port = data.port

  return dt
}

function updateDataSource (formData, dt) {
  dt.name = formData.name
  dt.type = formData.type

  dt = createDataSourceType(formData, dt)

  return dt
}
