var config = require('config')
var mailTemplates = config.get('mailTemplates')
var mailgunConfig = config.get('mailgun')
var mailgun = require('mailgun-js')({
  apiKey: mailgunConfig.apiKey,
  domain: mailgunConfig.domain
})

/**
 * type = mailTemplates
 * data = !recepient, company, hash
 */
exports.send = function (type, data) {
  process.nextTick(function () {
    var mailConfig = mailTemplates[type]
    var context = mailConfig.html

    var modifiedContext = getMailTemplateContext(type, context, data)

    var newData = {
      from: 'DataCircle <noreply@datacircle.io>',
      to: data.recepient,
      subject: mailConfig.subject,
      html: modifiedContext
    }

    mailgun.messages().send(newData, function (err, body) {
      if (err) {
        console.log(err)
      }
    })
  })
}

function getMailTemplateContext (type, context, data) {
  var modifiedContext = context
  modifiedContext = modifiedContext.replace('$url', config.url)

  switch (type) {
    case 'invitation':
      modifiedContext = modifiedContext
        .replace('$userHash', data.userHash)
        .replace('$company', data.company)
      break
    case 'accountVerification':
      modifiedContext = modifiedContext
        .replace('$userVericifationKey', data.verificationKey)
      break
  }

  return modifiedContext
}
