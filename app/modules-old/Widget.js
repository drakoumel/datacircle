var Widget = require('../models/Widget')
var Dashboard = require('../modules/Dashboard')
var Data = require('../models/Data')

module.exports = {
  detailsWidget: detailsWidget,
  updateOrCreateWidget: updateOrCreateWidget,
  availableWidgets: availableWidgets,
  delete: remove
}

function remove (req, callback) {
  process.nextTick(function () {
    var promise = Widget.findOne({
      'company': req.user.metadata.company,
      '_id': req.params.widgetId
    })

    promise
      .then(function (widget) {
        Dashboard.massRemoveWidget(req.user._id, req.params.widgetId)
        return widget
      })
      .then(function (widget) {
        widget.remove()
      })
      .then(function () {
        callback({
          data: null,
          msg: 'Widget was deleted.',
          code: 'success'
        })
      })
      .catch(function (err) {
        callback({
          data: null,
          msg: 'Could not delete widget.',
          code: 'error'
        })
      })
  })
}

function updateOrCreateWidget (req, callback) {
  process.nextTick(function () {
    var formData = req.body
    Widget.findOne({
      'company': req.user.metadata.company,
      '_id': formData._id
    })
      .exec(function (err, widget) {
        if (err) {
          console.log(err)
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (widget) {
          editWidget(req, callback)
        } else {
          registerWidget(req, callback)
        }
      })
  })
}

function editWidget (req, callback) {
  var formData = req.body
  formData.user = req.user

  isWidgetDataValid(req, callback)

  Widget.findOne({
    'company': req.user.metadata.company,
    '_id': formData._id
  })
    .exec(function (err, widget) {
      if (err) {
        callback({
          msg: 'There was an unexpected error!' + err,
          code: 'error'
        })
      } else if (widget) {
        widget = updateWidget(formData, widget)
        deleteCache(widget)
        widget.save(function (err) {
          if (err) {
            callback({
              msg: 'There was an unexpected error!',
              code: 'error'
            })
          }
          callback({
            msg: 'acknowledged',
            code: 'success'
          })
        })
      } else {
        callback({
          msg: 'No such widget found',
          code: 'error'
        })
      }
    })
}

function registerWidget (req, callback) {
  var formData = req.body
  isWidgetDataValid(req, callback)

  var newWidget = createWidget(formData)
  newWidget.owner = req.user._id
  newWidget.company = req.user.metadata.company

  newWidget.save(function (err) {
    if (err) {
      callback({
        msg: 'There was an unexpected error!',
        code: 'error'
      })
    }

    callback({
      msg: 'acknowledged',
      code: 'success'
    })
  })
}

function detailsWidget (req, callback) {
  process.nextTick(function () {
    if (req.params.widgetId) {
      Widget.findOne({
        'company': req.user.metadata.company,
        '_id': req.params.widgetId
      })
        .populate('role')
        .exec(function (err, widget) {
          if (err) {
            callback({
              data: null,
              msg: 'There was an unexpected error!',
              code: 'error'
            })
          } else if (widget) {
            callback({
              data: widget,
              msg: 'acknowledged',
              code: 'success'
            })
          }
        })
    } else {
      callback({
        data: null,
        msg: 'No such widget found',
        code: 'error'
      })
    }
  })
}

function availableWidgets (req, callback) {
  process.nextTick(function () {
    Widget.find({
      'company': req.user.metadata.company
    })
      .exec(function (err, widget) {
        if (err) {
          callback({
            data: null,
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (widget) {
          callback({
            data: widget,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      })
  })
}

function isWidgetDataValid (req, callback) {
  req.assert('chartType', '').notEmpty()
  req.assert('availableDataSource', '').notEmpty()
  req.assert('sql', '').notEmpty()
  req.assert('name', '').notEmpty()

  if (req.validationErrors()) {
    callback({
      msg: 'Data input was not valid!',
      code: 'error'
    })
  }
}

function updateWidget (formData, widget) {
  widget.name = formData.name
  widget.widgetType = formData.widgetType
  widget.chartType = formData.chartType
  widget.dataSource = formData.availableDataSource
  widget.query = formData.sql

  return widget
}

function createWidget (formData) {
  var newWidget = new Widget()
  newWidget.name = formData.name
  newWidget.widgetType = formData.widgetType
  newWidget.chartType = formData.chartType
  newWidget.dataSource = formData.availableDataSource
  newWidget.query = formData.sql

  return newWidget
}

function deleteCache (widget) {
  Data.findOneAndRemove({
    '_id': widget.dataId
  }).exec()
}
