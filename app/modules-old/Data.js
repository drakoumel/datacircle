var DataNormalizerMySQL = require('./DataNormalizer/MySQL.js')
var DataNormalizerMongoDB = require('./DataNormalizer/MongoDB.js')
var DataSource = require('../models/DataSource')
var Widget = require('../models/Widget')
var DataModel = require('../models/Data')
var mysql = require('mysql')
var MongoClient = require('mongodb').MongoClient

exports.getData = function (req, callback) {
  Widget.findOne({
    'company': req.user.metadata.company,
    '_id': req.params.widgetId
  })
    .populate('dataSource')
    .exec(function (err, widget) {
      if (err) {
        callback({
          data: null,
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      } else if (widget) {
        poolData(widget, callback)
      } else {
        callback({
          data: null,
          msg: 'No data',
          code: 'error'
        })
      }
    })
}

exports.dataInput = function (req, callback) {
  var formData = {
    userToken: req.postData.token,
    ds_id: req.postData.ds_id,
    type: req.postData.type,
    data: req.postData.data
  }

  if ((typeof formData.userToken === 'undefined' || formData.userToken == null) ||
    (typeof formData.data === 'undefined' || formData.data == null)) {
    callback({
      msg: 'Please provide all required fields (token,data,add)',
      code: -1
    })
  }

  if (typeof formData.type === 'undefined' || formData.type == null) {
    formData.type = 1 // default Raw
  } else if (formData.type !== 1) {
    // return { msg: "We currently only support Raw Type data input via post.", code: 2}
  }

  if (typeof formData.createWidget === 'undefined' || formData.createWidget == null) {
    formData.createWidget = false
  }
  if (typeof formData.name === 'undefined' || formData.name == null) {
    formData.name = 'create Random Name here'
  }

  if (formData.data !== 'undefined' && formData.data != null && formData.data.length > 0) {
    var data = new DataModel()
    data.data = formData.data
    data.save(function () {
      DataSource.updateDataRef(formData.ds_id, data._id)
    })

    callback({
      msg: 'acknowledged',
      code: 'success'
    })
  } else {
    callback({
      msg: 'empty data',
      code: 'error'
    })
  }
}

function findById (id, callback) { // eslint-disable-line
  DataSource.findOne({
    '_id': id
  })
    .exec(function (err, data) {
      if (err) {
        callback({
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      } else if (data) {
        callback(data)
      }
    })
}

function poolData (enhancedWidgetModel, callback) {
  var promise = DataModel.findOne({
    '_id': enhancedWidgetModel.dataId
  })
  promise
    .then(function (data) {
      if (data) {
        callback({
          data: data.data,
          msg: 'acknowledged',
          code: 'success'
        })
      } else {
        if (enhancedWidgetModel.dataSource === null) {
          callback({
            data: null,
            msg: 'Data source not found.',
            code: 'error'
          })
        } else {
          poolCase(enhancedWidgetModel, callback)
        }
      }
    })
}

function poolCase (enhancedWidgetModel, callback) {
  switch (enhancedWidgetModel.dataSource.type) {
    case 'mysql':
      poolMysql(enhancedWidgetModel, callback)
      break
    case 'mongodb':
      poolMongoDb(enhancedWidgetModel, callback)
      break
    default:
      poolMysql(enhancedWidgetModel, callback)
      break
  }
}

function poolMongoDb (enhancedWidgetModel, callback) {
  //  mongoose.connect('mongodb://username:password@host:port/database?options...')
  var uri = 'mongodb://' +
    enhancedWidgetModel.dataSource.mongodb.username + ':' +
    enhancedWidgetModel.dataSource.mongodb.password + '@' +
    enhancedWidgetModel.dataSource.mongodb.host + ':' +
    enhancedWidgetModel.dataSource.mongodb.port + '/' +
    enhancedWidgetModel.dataSource.mongodb.database + ''

  MongoClient.connect(uri, function (err, db) {
    if (err) {
      callback({
        data: null,
        msg: 'Error connection to datasource',
        code: 'error'
      })
    }

    var container = {}

    container.query = enhancedWidgetModel.query.trim()
    container.collectionName = container.query.substr(0, container.query.indexOf('.'))
    container.collection = db.collection(container.collectionName)
    container.action = container.query.substring(container.query.indexOf('.') + 1, container.query.indexOf('('))

    var queryStartIndex = container.query.indexOf('(') + 1
    var queryEndIndex = container.query.length - 1
    if (container.action === 'aggregate' && container.query.indexOf('[') > 0) {
      queryStartIndex = container.query.indexOf('[') + 1
      queryEndIndex = container.query.lastIndexOf('}') + 1
    }

    container.query = container.query.substring(queryStartIndex, queryEndIndex)
    container.query = container.query.replace(new RegExp(' ', 'g'), '')

    var strs = container.query.split('},')
    var jsonArray = []

    try {
      strs.forEach(function (item, index) {
        if (index < strs.length - 1) {
          item += '}'
        }
        jsonArray.push(JSON.parse(item))
      })
    } catch (ex) {
      callback({
        data: null,
        msg: ex + '',
        code: 'error'
      })
      return
    }

    if (container.action === 'aggregate') {
      container.query = jsonArray
    } else {
      container.query = jsonArray[0]
    }

    var query = (container.collection)[container.action](container.query)

    var afterFunction = 'toArray'
    if (container.action === 'findOne') {
      afterFunction = 'then'
    }

    try {
      query[afterFunction](function (err, data) {
        if (err) {
          console.log(err)
          callback({
            data: null,
            msg: err + '',
            code: 'error'
          })
        } else if (data) {
          var dataModified = DataNormalizerMongoDB.process(enhancedWidgetModel.chartType, data)
          saveData(dataModified, enhancedWidgetModel)
          callback({
            data: dataModified,
            msg: 'acknowledged',
            code: 'success'
          })
        }
        db.close()
      })
    } catch (ex) {
      console.log(ex)
      callback({
        data: null,
        msg: ex + '',
        code: 'error'
      })
      db.close()
    }
  })
}

function poolMysql (enhancedWidgetModel, callback) {
  var connection = mysql.createConnection({
    host: enhancedWidgetModel.dataSource.mysql.host,
    user: enhancedWidgetModel.dataSource.mysql.username,
    password: enhancedWidgetModel.dataSource.mysql.password,
    database: enhancedWidgetModel.dataSource.mysql.database
  })

  connection.query(enhancedWidgetModel.query, function (err, rows, fields) {
    connection.end(function (err) {
      if (err) {
        console.log(err)
      }
    })

    if (err) {
      console.log(err)
      callback({
        data: null,
        msg: 'error',
        code: 'error'
      })
    } else if (rows) {
      var dataModified = DataNormalizerMySQL.process(enhancedWidgetModel.chartType, rows)
      saveData(dataModified, enhancedWidgetModel)
      callback({
        data: dataModified,
        msg: 'acknowledged',
        code: 'success'
      })
    }
  })
}

function saveData (dataXY, enhancedWidgetModel) {
  var newData = new DataModel()
  newData.data = dataXY
  newData.save(function (err) {
    if (!err) {
      enhancedWidgetModel.dataId = newData._id
      enhancedWidgetModel.save(function (err) {
        if (err) {
          console.log(err)
        }
      })
    }
  })
}
