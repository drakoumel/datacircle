var Dashboard = require('../models/Dashboard')

exports.removeWidget = function (req, callback) {
  var promise = Dashboard.findOne({
    'owner': req.user._id,
    'widgets': {
      '$in': [
        req.params.widgetId
      ]
    }
  })

  promise
    .then(function (dashboard) {
      if (dashboard) {
        dashboard.widgets = dashboard.widgets.filter(function (i) {
          return i !== req.params.widgetId
        })
        dashboard.save()
      }

      callback({
        data: null,
        msg: 'The widget has been succesfully removed from the dashboard.',
        code: 'success'
      })
    })
    .catch(function (err, callback) {
      callback({
        data: err,
        msg: 'There was an error removing the widget.',
        code: 'error'
      })
    })
}

exports.massRemoveWidget = function (widgetId, callback) {
  var promise = Dashboard.find({
    'widgets': {
      '$in': [
        widgetId
      ]
    }
  })

  promise
    .then(function (dashboards) {
      if (dashboards) {
        dashboards.forEach(function (dashboard, index) {
          dashboard.widgets = dashboard.widgets.filter(function (i) {
            return i !== widgetId
          })
          dashboard.save()
        })
      }
    })
    .catch(function (err) {
      return false
    })
}

exports.getWidgets = function (req, callback) {
  var promise = Dashboard.findOne({
    'owner': req.user._id
  })
  promise
    .then(function (dashboard) {
      if (dashboard) {
        callback({
          data: dashboard,
          msg: 'acknowledged',
          code: 'success'
        })
      } else {
        callback({
          data: null,
          msg: 'acknowledged',
          code: 'success'
        })
      }
    })
    .catch(function (err) {
      console.error(err)
      callback({
        data: null,
        msg: 'Unexpected error',
        code: 'error'
      })
    })
}

exports.addWidget = function (req, callback) {
  var promise = Dashboard.findOne({
    'owner': req.user._id
  })
  promise
    .then(function (dashboard) {
      if (dashboard) {
        if (contains.call(dashboard.widgets, req.postData.widgetId)) {
          callback({
            data: dashboard,
            msg: 'Already Exists',
            code: 'invalid'
          })
        } else {
          dashboard.widgets.push(req.postData.widgetId)
          dashboard.save()

          callback({
            data: dashboard,
            msg: 'acknowledged',
            code: 'success'
          })
        }
      } else {
        var ds = new Dashboard()
        ds.owner = req.user._id
        ds.widgets.push(req.postData.widgetId)
        ds.save()

        callback({
          data: ds,
          msg: 'acknowledged',
          code: 'success'
        })
      }
    })
    .catch(function (err) {
      console.error(err)
      callback({
        data: null,
        msg: 'Unexpected error',
        code: 'error'
      })
    })
}

var contains = function (needle) {
  // Per spec, the way to identify NaN is that it is not equal to itself
  var findNaN = needle !== needle // eslint-disable-line
  var indexOf

  if (!findNaN && typeof Array.prototype.indexOf === 'function') {
    indexOf = Array.prototype.indexOf
  } else {
    indexOf = function (needle) {
      var i = -1
      var index = -1

      for (i = 0; i < this.length; i++) {
        var item = this[i]

        if ((findNaN && item !== item) || item === needle) { // eslint-disable-line
          index = i
          break
        }
      }

      return index
    }
  }

  return indexOf.call(this, needle) > -1
}
