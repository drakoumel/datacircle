var easyPbkdf2 = require('easy-pbkdf2')()
var User = require('../models/User')
var RoleHandler = require('../modules/RoleHandler')
var Mail = require('../modules/Mail')
var crypto = require('crypto')
var base64url = require('base64url')

exports.findUserByToken = function (token, callback) {
  process.nextTick(function () {
    var promise = User.findOne({
      'token': token
    })
    promise
      .then(function (ds) {
        callback(ds)
      })
      .catch(function (err) {
        console.error(err)
        callback(null)
      })
  })
}

exports.findUserByEmail = function (email, callback) {
  process.nextTick(function () {
    User.findOne({
      'email': email
    }, function (err, user) {
      if (err) {
        callback(null)
      } else if (user) {
        callback(user)
      } else {
        callback(null)
      }
    })
  })
}

exports.user = function (req) {
  process.nextTick(function () {
    var Username = req.params.username
    User.findOne({
      'valid.email': Username
    }, function (err, user) {
      if (err) {
        return {
          url: '/',
          profile: null,
          user: null,
          msg: 'There was an unexpected error!',
          code: 'error'
        }
      }
      if (user) {
        return {
          url: '/',
          profile: user,
          user: req.user,
          msg: 'acknowledged2',
          code: 'success'
        }
      }
    })
  })
}

exports.userInvite = function (req, callback) {
  process.nextTick(function () {
    var newUser = new User()
    newUser.hash = randomStringAsBase64Url(20)
    newUser.metadata.company = req.user.metadata.company
    newUser.role = req.body._id
    newUser.save(function (err) {
      if (err) {
        callback({
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      } else {
        var data = {
          'recepient': req.body.Email,
          'userHash': newUser.hash,
          'company': newUser.metadata.company
        }
        Mail.send('invitation', data)
        callback({
          msg: 'An email has been sent to the provided address.',
          code: 'success'
        })
      }
    })
  })
}

exports.verify = function (req, callback) {
  process.nextTick(function () {
    if (req.params.key) {
      User.findOne({
        'verification.key': req.params.key
      })
        .populate('role')
        .exec(function (err, user) {
          if (err) {
            callback({
              url: '/',
              msg: 'There was an unexpected error!',
              code: 'error'
            })
          } else if (user) {
            user.verification.key = ''
            user.verification.activated = true
            user.save(function (err) {
              if (err) {
                callback({
                  url: '/',
                  msg: 'There was an unexpected error!',
                  code: 'error'
                })
              } else {
                callback({
                  url: '/',
                  user: user,
                  msg: 'Your Email address has been succesfully verified!',
                  code: 'success'
                })
              }
            })
          } else {
            callback({
              url: '/',
              msg: 'No such verification code found.',
              code: 'error'
            })
          }
        })
    }
  })
}

exports.login = function (req, callback) {
  process.nextTick(function () {
    if (isLoginDataValid(req)) {
      User.findOne({
        'valid.email': req.body.Email
      }, function (err, user) {
        if (err) {
          callback({
            url: '/#login',
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        }
        if (!user) {
          callback({
            url: '/#login',
            msg: 'No such email/password combination was found',
            code: 'error'
          })
        }
        if (user) {
          easyPbkdf2.verify(user.valid.salt, user.valid.password, req.body.Password, function (err, valid) {
            if (err) {
              console.error(err)
              callback({
                url: '/#login',
                msg: 'Unexpected error has occured',
                code: 'error'
              })
            }
            if (!valid) {
              callback({
                url: '/#login',
                msg: 'No such email/password combination was found',
                code: 'error'
              })
            } else {
              user.valid.salt = null
              user.valid.password = null
              callback({
                user: user,
                url: '/',
                msg: 'acknowledged',
                code: 'success'
              })
            }
          })
        }
      })
    } else {
      callback({
        url: '/#login',
        msg: 'Invalid Data',
        code: 'error'
      })
    }
  })
}

exports.register = function (req, callback) {
  process.nextTick(function () {
    if (req.body.Hash.length > 1) {
      registerInvitedUser(req, callback)
    } else {
      registerNewUser(req, callback)
    }
  })
}

function registerInvitedUser (req, callback) {
  process.nextTick(function () {
    if (!req.user && isCompleteRegistrationDataValid(req)) {
      validatePassword(req, callback)

      User.findOne({
        'valid.email': req.body.Email
      }, function (err, user) {
        if (err) {
          callback({
            url: '/#register',
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        } else if (user) {
          callback({
            url: '/#register',
            msg: 'Email account already registed.',
            code: 'error'
          })
        }
      })

      User.findOne({
        'metadata.company': new RegExp(req.body.Company, 'i'), 'hash': req.body.Hash, 'isActive': false
      }, function (err, user) {
        if (err) {
          callback({
            url: '/#register',
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        }
        if (user) {
          updateInvitedUser(user, req, callback)
        } else {
          callback({
            url: '/#register',
            msg: 'No such company found.',
            code: 'error'
          })
        }
      })
    } else {
      callback({
        url: '/#register',
        msg: 'User already logged in.',
        code: 'error'
      })
    }
  })
}

function updateInvitedUser (user, req, callback) {
  user.valid.email = req.body.Email
  user.isActive = true
  user.metadata.ln = req.body.LN
  user.metadata.fn = req.body.FN
  user.verification.key = randomStringAsBase64Url(20)
  user.token = randomStringAsBase64Url(20)

  easyPbkdf2.secureHash(req.body.Password, easyPbkdf2.generateSalt(), function (err, passwordHash, originalSalt) {
    if (err) {
      callback({
        url: '/#register',
        msg: 'There was an unexpected error!',
        code: 'error'
      })
    }

    user.valid.salt = originalSalt
    user.valid.password = passwordHash
    user.save(function (err) {
      if (err) {
        callback({
          url: '/#register',
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      } else {
        var data = {
          'recepient': user.valid.email,
          'verificationKey': user.verification.key
        }
        Mail.send('accountVerification', data)
        callback({
          user: user,
          url: '/',
          msg: 'You have been succesfully registered. Please validate your email address.',
          code: 'success'
        })
      }
    })
  })
}

function registerNewUser (req, callback) {
  process.nextTick(function () {
    if (!req.user && isRegisterDataValid(req)) {
      validatePassword(req, callback)

      User.findOne({
        $or: [ { 'valid.email': req.body.Email }, { 'metadata.company': new RegExp(req.body.Company, 'i') } ]
      }, function (err, user) {
        if (err) {
          callback({
            url: '/#register',
            msg: 'There was an unexpected error!',
            code: 'error'
          })
        }
        if (user) {
          callback({
            url: '/#register',
            msg: 'The Email address or Company is already in use.',
            code: 'error'
          })
        } else {
          createNewUser(req, callback)
        }
      })
    } else {
      callback({
        url: '/#register',
        msg: 'User already logged in.',
        code: 'error'
      })
    }
  })
}

function createNewUser (req, callback) {
  var newUser = new User()
  newUser.valid.email = req.body.Email
  newUser.isActive = true
  newUser.metadata.ln = req.body.LN
  newUser.metadata.fn = req.body.FN
  newUser.metadata.company = req.body.Company
  newUser.verification.key = randomStringAsBase64Url(20)
  newUser.token = randomStringAsBase64Url(20)

  newUser.role = RoleHandler.getAdminRoleDummy(newUser._id, newUser.metadata.company)

  easyPbkdf2.secureHash(req.body.Password, easyPbkdf2.generateSalt(), function (err, passwordHash, originalSalt) {
    if (err) {
      callback({
        url: '/#register',
        msg: 'There was an unexpected error!',
        code: 'error'
      })
    }

    newUser.valid.salt = originalSalt
    newUser.valid.password = passwordHash
    newUser.save(function (err) {
      if (err) {
        callback({
          url: '/#register',
          msg: 'There was an unexpected error!',
          code: 'error'
        })
      } else {
        var data = {
          'recepient': newUser.valid.email,
          'verificationKey': newUser.verification.key
        }
        Mail.send('accountVerification', data)
        callback({
          user: newUser,
          url: '/',
          msg: 'You have been succesfully registered. Please validate your email address.',
          code: 'success'
        })
      }
    })
  })
}

function validatePassword (req, callback) {
  if (req.body.Password !== req.body.RePassword) {
    callback({
      url: '/#register',
      msg: 'Passwords do not match',
      code: 'error'
    })
  }
}

function randomStringAsBase64Url (size) {
  return base64url(crypto.randomBytes(size))
}

function isRegisterDataValid (req, callback) {
  req.assert('Email', '').notEmpty()
  req.assert('Password', '').notEmpty()
  req.assert('RePassword', '').notEmpty()
  req.assert('FN', '').notEmpty()
  req.assert('LN', '').notEmpty()
  req.assert('Company', '').notEmpty()

  if (req.validationErrors()) {
    return false
  }
  return true
}

function isCompleteRegistrationDataValid (req, callback) {
  req.assert('Email', '').notEmpty()
  req.assert('Password', '').notEmpty()
  req.assert('RePassword', '').notEmpty()
  req.assert('FN', '').notEmpty()
  req.assert('LN', '').notEmpty()
  req.assert('Hash', '').notEmpty()

  if (req.validationErrors()) {
    return false
  }
  return true
}

function isLoginDataValid (req) {
  req.assert('Email', '').notEmpty()
  req.assert('Password', '').notEmpty()
  if (req.validationErrors()) {
    return false
  }
  return true
}
