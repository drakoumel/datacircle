'use strict'

module.exports = function (router) {
  router.get('/', (req, res) => {
    res.render('index.html', {
      user: req.user,
      createRole: require('./models/Role').getEmptyRoles(),
      title: 'DataCircle'
    })
  })

  router.get('/acknowledgement', require('./controllers/heartbeat').test)

  router.get('/robots.txt', function (req, res) {
    res.render('public/robots.txt')
  })

  router.get('/changelog', function (req, res) {
    var fs = require('fs')
    var marked = require('marked')

    fs.readFile('./CHANGELOG.md', 'utf8', function (err, data) {
      if (err) {
        res.render('generic/changelog.html', {
          title: 'DataCircle - Changelog',
          data: 'error'
        })
      }
      res.render('generic/changelog.html', {
        title: 'DataCircle - Changelog',
        data: marked(data)
      })
    })
  })

  router.get('/contact', function (req, res) {
    res.render('generic/underConstructionPage.html', {
      user: req.user,
      title: 'DataCircle'
    })
  })
  router.get('/jobs', function (req, res) {
    res.render('generic/underConstructionPage.html', {
      user: req.user,
      title: 'DataCircle - Jobs'
    })
  })
  router.get('/blog', function (req, res) {
    res.render('generic/underConstructionPage.html', {
      title: 'DataCircle - Blog'
    })
  })
  router.get('/privacy', function (req, res) {
    res.render('generic/underConstructionPage.html', {
      title: 'DataCircle - Privacy'
    })
  })
  router.get('/tou', function (req, res) {
    res.render('generic/underConstructionPage.html', {
      title: 'DataCircle - Terms of Use'
    })
  })

  // role
  router.put('/role', require('./controllers/role').createOrUpdateRole)
  router.get('/roles', require('./controllers/role').getRoles)
  // router.get('/role/:roleId', require('./controllers/role').getRole)

  // // widgets
  // router.get('/api/widget/', isLoggedIn, require('./controllers/widget').getWidget)
  router.get('/availableWidgets', isLoggedIn, require('./controllers/widget').availableWidgets)
  router.post('/updateOrCreateWidget', isLoggedIn, require('./controllers/widget').updateOrCreateWidget)
  router.get('/detailsForWidget/:widgetId', isLoggedIn, require('./controllers/widget').detailsWidget)
  router.delete('/widget/:widgetId', isLoggedIn, require('./controllers/widget').deleteWidget)
  //
  // // widget configuration
  router.get('/datasources', isLoggedIn, require('./controllers/datasource').getAvailableDataSources)
  router.get('/chartType', isLoggedIn, require('./controllers/chart').getChartTypes)
  router.get('/dataSourceType', isLoggedIn, require('./controllers/datasource').getDataSourceTypes)
  router.get('/detailsForDataSource/:dataSourceId', isLoggedIn, require('./controllers/datasource').detailsDataSource)
  router.post('/updateOrCreateDataSource', isLoggedIn, require('./controllers/datasource').updateOrCreateDataSource)
  router.get('/widgetData/:widgetId', isLoggedIn, require('./controllers/cache').get)
  //
  // dashboards
  router.put('/dashboard', isLoggedIn, require('./controllers/dashboard').createOrUpdate)
  router.get('/dashboard/getAll', isLoggedIn, require('./controllers/dashboard').getAll)
  router.get('/dashboard/widgets/:dashboardId', isLoggedIn, require('./controllers/dashboard').getWidgetsForDashboard)
  router.put('/dashboard/addWidget', isLoggedIn, require('./controllers/dashboard').add)
  router.post('/dashboard/removewidget', isLoggedIn, require('./controllers/dashboard').remove)

  // user
  router.post('/login', require('./controllers/user').login)
  router.post('/register', require('./controllers/user').register)
  router.get('/verify/:key', require('./controllers/user').verify)
  router.post('/userinvite', require('./controllers/user').userInvite)
  router.post('/updateSelectedDashbaord', require('./controllers/user').updateSelectedDashbaord)
  router.get('/logout', function (req, res) {
    req.session.destroy()
    req.logout()
    res.redirect('/')
  })

  // data
  //router.post('/data', require('./controllers/data').dataInput)
}

function isLoggedIn (req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }
  res.redirect('/')
}
