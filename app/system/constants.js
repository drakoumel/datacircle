'use strict'

module.exports = {
  ERROR: Symbol('error'),
  SUCCESS: Symbol('success'),
  NO_PRIVILEDGE: Symbol('No Priviledge')
}
