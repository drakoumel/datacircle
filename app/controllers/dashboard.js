var Dashboard = require('../models/Dashboard')

exports.add = function (req, res) {
  Dashboard.addWidget(req, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}

exports.remove = function (req, res) {
  Dashboard.removeWidget(req, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}

exports.getAll = function (req, res) {
  Dashboard.getDashboardsWithWidgets(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.getWidgetsForDashboard = function (req, res) {
  Dashboard.getWidgetsForDashboard(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.createOrUpdate = function (req, res) {
  Dashboard.createOrUpdate(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}
