var DataModule = require('../models/Data')

exports.getData = function (req, res) {
  DataModule.getData(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.dataInput = function (req, res) {
  DataModule.dataInput(req, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}
