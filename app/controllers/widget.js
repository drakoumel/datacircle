'use strict'
// modules/widget.js
const Widget = require('../models/Widget')

exports.deleteWidget = function (req, res) {
  Widget.delete(req, function (response) {
    req.flash(response.code, response.msg)
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.updateOrCreateWidget = function (req, res) {
  Widget.updateOrCreateWidget(req, function (response) {
    req.flash(response.code, response.msg)
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.detailsWidget = function (req, res) {
  Widget.detailsWidget(req, function (response) {
    req.flash(response.code, response.msg)
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.getWidget = function (req, res) {
  Widget.getWidget(req, function (response) {
    req.flash(response.code, response.msg)
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.availableWidgets = function (req, res) {
  Widget.availableWidgets(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}
