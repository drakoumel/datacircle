const Cache = require('../models/Cache')

exports.get = function (req, res) {
  let widgetId = req.params.widgetId
  let callbackf = function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  }

  if (widgetId) {
    Cache.get(widgetId, callbackf)
  } else {
    callbackf({ data: null, message: 'No widget id given.', code: 'error' })
  }
}
