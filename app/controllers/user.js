'use strict'
// controllers/user.js
const User = require('../models/Account')
const AccountWF = require('../workflows/Account')
const RoleHandler = require('../models/Role')

exports.verify = function (req, res) {
  User.verify(req, function (response) {
    req.flash(response.code, response.msg)
    req.session.user = response.user
    res.redirect(response.url)
  })
}

exports.login = function (req, res) {
  let callbackf = function (response) {
    req.flash(response.code, response.msg)
    req.session.user = response.user
    res.redirect(response.url || response.data)
  }

  if (User._isLoginDataValid(req)) {
    User.login(req.body, callbackf)
  } else {
    callbackf({
      url: '/#login',
      msg: 'Form data invalid.',
      code: 'error'
    })
  }
}

exports.register = function (req, res) {
  let callbackf = function (response) {
    req.flash(response.code, response.msg)
    req.session.user = response.user
    res.redirect(response.url)
  }

  if (req.body.Hash.length > 1) {
    User._registerInvitedUser(req, callbackf)
  } else {
    if (req.user) {
      callbackf({
        url: '/#register',
        msg: 'User already logged in.',
        code: 'error'
      })
      return
    }
    if (!User.isRegisterDataValid(req)) {
      callbackf({
        url: '/#register',
        msg: 'Form data invalid.',
        code: 'error'
      })
      return
    }
    if (!User.isRePasswordValid(req)) {
      callbackf({
        url: '/#register',
        msg: 'Passwords do not match',
        code: 'error'
      })
      return
    }

    AccountWF.createNewUser(req.body, callbackf)
  }
}

exports.userInvite = function (req, res) {
  let email = req.body.Email
  let company = req.user.company
  let roleId = req.body.roleId

  User.userInvite(email, company, roleId, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}

exports.renderSettingsPage = function (req, res) {
  res.render('user/administration/settings.ejs', {
    createRole: RoleHandler.getEmptyRole(),
    title: 'DataCircle - Settings'
  })
}

exports.updateSelectedDashbaord = function (req, res) {
  let selectedDashboard = req.body.dashboardId

  User.updateSelectedDashbaord(selectedDashboard, req.user, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}
