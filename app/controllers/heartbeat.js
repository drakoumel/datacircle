'use strict'
// controllers/chart.js

const Heartbeat = require('../models/Heartbeat')

exports.test = function (req, res) {
  Heartbeat.test(function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}
