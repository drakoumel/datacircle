// controllers/role.js
var Role = require('../models/Role')

exports.createOrUpdateRole = function (req, res) {
  Role.createOrUpdateRole(req, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}

exports.getRoles = function (req, res) {
  Role.getRoles(req.user, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}
