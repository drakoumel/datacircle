var DataSource = require('../models/DataSource')

exports.getData = function (req, res) {
  DataSource.getData(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.updateOrCreateDataSource = function (req, res) {
  DataSource.updateOrCreateDataSource(req, function (response) {
    res.end(JSON.stringify({ message: response.msg, code: response.code }))
  })
}

exports.getDataSourceTypes = function (req, res) {
  DataSource.getDataSourceTypes(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.getAvailableDataSources = function (req, res) {
  DataSource.getAvailableDataSourcesEnhanced(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}

exports.detailsDataSource = function (req, res) {
  DataSource.getDataSourceDetails(req, function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}
