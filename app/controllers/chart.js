// controllers/chart.js

var Chart = require('../models/Chart')

exports.getChartTypes = function (req, res) {
  Chart.getChartTypes(function (response) {
    res.end(JSON.stringify({ data: response.data, message: response.msg, code: response.code }))
  })
}
