### Changelog

##### 3.2.2
  Enhanced MongoDB module to handle multiple $group/jsonObjects.

##### 3.1.2
  Minor fixes
    Hex Color temporary fix
    Home page js fix
  Email further abstraction

##### 3.1.1
  First iteration of NoSQL (MongoDB) query process.
    Tested .find() and .aggregate() functions
  Removed Development notice and replaced with Beta.
  Added demoData connectionStrings

##### 3.0.1
  Fixed issue with landing chart being loaded on dashboard.

##### 3.0.0
  Big feature spree including:
    Widget header.
    Widget removal from dashboard.
    Widget print fix.
    Widget refresh on demand.
    Widget auto-refresh based on interval.
    Widget delete.
    Email module change.
    Added logo.

##### 2.2.2

  Magic Navigation Binding.
  Anchor functionality for Basic actions.
  Side-Nav Mobile Ready.
  Index.js size reduction from 7kb to 2.7kb

##### 2.2.1

  Removed x-powered-by header from response.

##### 2.2.0

  Dynamic range for area and bar charts.
  Automated color generator.

##### 2.1.4

  Added 404 page.

##### 2.1.3

  Remove debug code.

##### 2.1.2

  Added changelog view.

##### 2.1.1

  BugFix - Added missing response message upon creation of widget.

##### 2.1.0

  DataNormalization from query output is moved to a different module.
  Added extra charts (pie & donut).
  Chart rendering is not a stand alone library.

##### 2.0.2

  Added invitation to company network through email invite.

##### 2.0.1

  Applied Javascript Style Standard based on http://standardjs.com

##### 2.0.0

  Rename Application to DataCircle from MyMonitor.

##### 1.0.1

  Added environment configs.
  Clean up server.js

##### 1.0.0

  First official working release.
